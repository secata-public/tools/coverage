package com.secata.tools.thread;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test of {@link ExecutionFactoryFake} methods and structure. */
class ExecutionFactoryFakeTest {

  private static final String THREAD_TEST_NAME = "Thread-test-name";
  private ExecutionFactoryFake executionFactory;

  /** Sets up the tests. */
  @BeforeEach
  void setUp() {
    executionFactory = ExecutionFactoryFake.create();
  }

  @Test
  void newSingleThreadExecutor() throws Exception {
    ExecutorService executorService = executionFactory.newSingleThreadExecutor(THREAD_TEST_NAME);
    testExecutionService(executorService);
  }

  @Test
  void newCachedThreadPool() throws Exception {
    ExecutorService executorService = executionFactory.newCachedThreadPool(THREAD_TEST_NAME);
    testExecutionService(executorService);
  }

  @Test
  void newScheduledThreadPool() throws Exception {
    ExecutorService executorService = executionFactory.newScheduledThreadPool(THREAD_TEST_NAME, 10);
    testExecutionService(executorService);
  }

  @Test
  void newSingleThreadScheduledExecutor() throws Exception {
    ExecutorService executorService =
        executionFactory.newSingleThreadScheduledExecutor(THREAD_TEST_NAME);
    testExecutionService(executorService);
  }

  private void testExecutionService(ExecutorService executorService) throws Exception {
    AtomicInteger count = new AtomicInteger(1);
    Future<Integer> submit = executorService.submit(count::decrementAndGet);
    assertThat(count.get()).isEqualTo(1);
    assertThat(submit.isDone()).isFalse();
    TestExecutionQueue executionQueue = executionFactory.getExecutionQueue(THREAD_TEST_NAME);
    executionQueue.runStep();
    assertThat(submit.get()).isEqualTo(0);
    assertThat(count.get()).isEqualTo(0);
    assertThat(submit.isDone()).isTrue();
  }

  @Test
  void newSingleThreadExecutorMultipleItems() {
    ExecutorService executorService = executionFactory.newSingleThreadExecutor(THREAD_TEST_NAME);
    AtomicInteger count1 = new AtomicInteger(10);
    AtomicInteger count2 = new AtomicInteger(10);
    for (int i = 0; i < 10; i++) {
      Future<Integer> submit1 = executorService.submit(count1::decrementAndGet);
      assertThat(submit1.isCancelled()).isFalse();
      assertThat(submit1.isDone()).isFalse();
      Future<Integer> submit2 = executorService.submit(count2::decrementAndGet);
      assertThat(submit2.isDone()).isFalse();
    }
    TestExecutionQueue executionQueue = executionFactory.getExecutionQueue(THREAD_TEST_NAME);
    executionQueue.runStep(19);
    assertThat(count1.get()).isEqualTo(0);
    assertThat(count2.get()).isEqualTo(1);
    executionQueue.runStep();
    assertThat(count2.get()).isEqualTo(0);

    assertThat(executionFactory.getQueueNames()).hasSize(1).contains(THREAD_TEST_NAME);
  }

  @Test
  void multipleExecutors() {
    executionFactory.newSingleThreadExecutor(THREAD_TEST_NAME + "1");
    executionFactory.newSingleThreadExecutor(THREAD_TEST_NAME + "2");
    executionFactory.newSingleThreadExecutor(THREAD_TEST_NAME + "3");
    executionFactory.newSingleThreadExecutor(THREAD_TEST_NAME + "4");

    assertThat(executionFactory.getQueueNames()).hasSize(4).contains(THREAD_TEST_NAME + "3");
  }

  @Test
  void newSingleThreadExecutorCancelFuture() {
    ExecutorService executorService = executionFactory.newSingleThreadExecutor(THREAD_TEST_NAME);
    AtomicInteger count1 = new AtomicInteger(1);
    assertThat(executorService.isShutdown()).isFalse();
    assertThat(executorService.isTerminated()).isFalse();
    Future<Integer> submit1 = executorService.submit(count1::decrementAndGet);
    assertThat(submit1.isCancelled()).isFalse();
    assertThat(submit1.cancel(true)).isFalse();
    assertThat(submit1.isCancelled()).isTrue();
    assertThat(executorService.isShutdown()).isFalse();
    assertThat(executorService.isTerminated()).isFalse();
  }

  @Test
  void newSingleThreadExecutorCancelService() throws InterruptedException {
    ExecutorService executorService = executionFactory.newSingleThreadExecutor(THREAD_TEST_NAME);
    AtomicInteger count1 = new AtomicInteger(1);
    assertThat(executorService.isShutdown()).isFalse();
    assertThat(executorService.isTerminated()).isFalse();
    Runnable runnable = count1::decrementAndGet;
    Future<?> submit1 = executorService.submit(runnable);
    assertThat(submit1.isCancelled()).isFalse();
    List<Runnable> runnableList = executorService.shutdownNow();
    assertThat(executorService.awaitTermination(100, TimeUnit.SECONDS)).isFalse();
    assertThat(submit1.isCancelled()).isTrue();
    assertThat(runnableList).hasSize(1);
    assertThat(count1.get()).isEqualTo(1);
    runnableList.get(0).run();
    assertThat(count1.get()).isEqualTo(0);
    assertThat(executorService.isShutdown()).isTrue();
    assertThat(executorService.isTerminated()).isTrue();
  }

  @Test
  void submitWorkWithResult() throws InterruptedException, ExecutionException {
    ExecutorService executorService = executionFactory.newSingleThreadExecutor(THREAD_TEST_NAME);
    AtomicInteger count1 = new AtomicInteger(1);
    Future<?> submit1 =
        executorService.submit(
            () -> {
              count1.decrementAndGet();
            });
    Future<Integer> submit2 = executorService.submit(count1::decrementAndGet, 10);
    TestExecutionQueue executionQueue = executionFactory.getExecutionQueue(THREAD_TEST_NAME);
    executionQueue.runStep(3);
    assertThat(submit1.get()).isNull();
    assertThat(submit2.get()).isEqualTo(10);
  }

  @Test
  void abstractTestExecutorServiceInvokeAllNoWait()
      throws InterruptedException, ExecutionException {
    ExecutorService executorService = executionFactory.newSingleThreadExecutor(THREAD_TEST_NAME);
    List<Callable<Integer>> callables = getCountCallables();
    executionFactory.getExecutionQueue(THREAD_TEST_NAME).runStepAfterTime(11, 100_000);
    List<Future<Integer>> futures = executorService.invokeAll(callables);
    assertThat(futures).hasSize(10);
    for (Future<Integer> future : futures) {
      Integer actual = future.get();
      assertThat(actual).isNull();
    }
  }

  @Test
  void abstractTestExecutorServiceInvokeAllWait() throws InterruptedException, ExecutionException {
    ExecutorService executorService = executionFactory.newSingleThreadExecutor(THREAD_TEST_NAME);
    executionFactory.getExecutionQueue(THREAD_TEST_NAME).runStepAfterTime(22, 100_000);
    List<Callable<Integer>> callables = getCountCallables();
    List<Future<Integer>> futures = executorService.invokeAll(callables, 100, TimeUnit.SECONDS);
    assertThat(futures).hasSize(10);
    for (Future<Integer> future : futures) {
      Integer actual = future.get();
      assertThat(actual).isGreaterThan(10);
    }
  }

  @Test
  void abstractTestExecutorServiceInvokeAnyNoWait() throws Exception {
    ExecutorService executorService = executionFactory.newSingleThreadExecutor(THREAD_TEST_NAME);
    List<Callable<Integer>> callables = getCountCallables();
    executionFactory.getExecutionQueue(THREAD_TEST_NAME).runStepAfterTime(11, 0);
    Integer result = executorService.invokeAny(callables);
    assertThat(result).isEqualTo(11);
  }

  @Test
  void abstractTestExecutorServiceInvokeAnyWait() throws Exception {
    ExecutorService executorService = executionFactory.newSingleThreadExecutor(THREAD_TEST_NAME);
    List<Callable<Integer>> callables = getCountCallables();
    executionFactory.getExecutionQueue(THREAD_TEST_NAME).runStepAfterTime(11, 100_000);
    Integer result = executorService.invokeAny(callables, 100, TimeUnit.SECONDS);
    assertThat(result).isEqualTo(11);
  }

  private static List<Callable<Integer>> getCountCallables() {
    AtomicInteger count = new AtomicInteger(10);
    Callable<Integer> callable = count::incrementAndGet;
    List<Callable<Integer>> callables =
        IntStream.rangeClosed(0, 9).mapToObj(i -> callable).toList();
    return callables;
  }

  @Test
  void sortedScheduledFutures() throws Exception {
    ScheduledExecutorService executorService =
        executionFactory.newSingleThreadScheduledExecutor(THREAD_TEST_NAME);
    List<Callable<Integer>> countCallables =
        IntStream.rangeClosed(0, 9).mapToObj(i1 -> (Callable<Integer>) () -> i1).toList();

    long i = 0;
    List<ScheduledFuture<Integer>> list = new ArrayList<>();
    for (Callable<Integer> countCallable : countCallables) {
      list.add(executorService.schedule(countCallable, 10 - i, TimeUnit.SECONDS));
      i++;
    }
    Collections.sort(list);
    TestExecutionQueue executionQueue = executionFactory.getExecutionQueue(THREAD_TEST_NAME);
    executionQueue.simulateWaitTime(10000);
    executionQueue.runStep(10);
    int j = 9;
    for (ScheduledFuture<Integer> future : list) {
      assertThat(future.get()).isEqualTo(j);
      j--;
    }
  }

  @Test
  void scheduledExecutorSimpleSchedule() throws Exception {
    ScheduledExecutorService executorService =
        executionFactory.newSingleThreadScheduledExecutor(THREAD_TEST_NAME);
    AtomicInteger count = new AtomicInteger(20);
    ScheduledFuture<Integer> submitCallable =
        executorService.schedule(count::decrementAndGet, 120_000, TimeUnit.MILLISECONDS);
    final ScheduledFuture<Integer> fastCallable =
        executorService.schedule(count::decrementAndGet, 10, TimeUnit.SECONDS);
    assertThat(submitCallable.getDelay(TimeUnit.SECONDS)).isEqualTo(120);

    TestExecutionQueue executionQueue = executionFactory.getExecutionQueue(THREAD_TEST_NAME);
    executionQueue.runStepAfterTime(1, 10_000);
    ScheduledFuture<?> submitRunnable =
        executorService.schedule((Runnable) count::decrementAndGet, 1, TimeUnit.MINUTES);

    assertThatCode(() -> submitRunnable.get(1, TimeUnit.SECONDS))
        .hasMessage("No element available");

    executionQueue.runStep();
    assertThatCode(() -> submitCallable.get(10, TimeUnit.SECONDS))
        .hasMessage("No element available");
    assertThat(fastCallable.get()).isEqualTo(19);
    executionQueue.simulateWaitTime(109_999);
    executionQueue.runStep();
    assertThat(submitCallable.get()).isNull();
    executionQueue.simulateWaitTime(1);
    executionQueue.runStep();
    assertThat(submitCallable.get()).isEqualTo(17);
  }

  @Test
  void scheduledExecutorRecurringSchedule() {
    ScheduledExecutorService executorService =
        executionFactory.newSingleThreadScheduledExecutor(THREAD_TEST_NAME);
    TestExecutionQueue executionQueue = executionFactory.getExecutionQueue(THREAD_TEST_NAME);
    try {
      AtomicInteger count = new AtomicInteger(18);

      ScheduledFuture<?> callable =
          executorService.schedule(count::decrementAndGet, 1, TimeUnit.MINUTES);
      assertThat(callable.get()).isNull();
      ScheduledFuture<?> firstRunnable =
          executorService.scheduleAtFixedRate(count::decrementAndGet, 1, 1, TimeUnit.MINUTES);
      assertThatCode(() -> firstRunnable.get(1, TimeUnit.SECONDS))
          .hasMessage("No element available");

      ScheduledFuture<?> willNotRun =
          executorService.scheduleWithFixedDelay(() -> count.lazySet(5), 1, 2, TimeUnit.DAYS);
      assertThat(willNotRun.isDone()).isFalse();

      ScheduledFuture<?> submitRunnable =
          executorService.scheduleWithFixedDelay(count::decrementAndGet, 1, 2, TimeUnit.MINUTES);
      assertThatCode(() -> submitRunnable.get(1, TimeUnit.SECONDS))
          .hasMessage("No element available");

      assertThat(executionQueue.runStep()).isFalse();
      executionQueue.simulateWaitTime(57_999);
      assertThat(executionQueue.runStep()).isFalse();
      assertThat(willNotRun.isDone()).isFalse();
      executionQueue.simulateWaitTime(1);
      assertThat(callable.get()).isNull();
      assertThat(executionQueue.runStep(4)).isEqualTo(2);
      assertThat(callable.get(1, TimeUnit.SECONDS)).isEqualTo(17);

      assertThat(executionQueue.runStep(3)).isEqualTo(1);
      assertThat(executionQueue.runStep()).isFalse();
      assertThat(count.get()).isEqualTo(15);
      executionQueue.simulateWaitTime(60_000);
      assertThat(executionQueue.runStep(5)).isEqualTo(2);
      assertThat(count.get()).isEqualTo(13);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Test
  void scheduledExecutorStoppedAfterShutdown() {
    ScheduledExecutorService executorService =
        executionFactory.newSingleThreadScheduledExecutor(THREAD_TEST_NAME);
    TestExecutionQueue executionQueue = executionFactory.getExecutionQueue(THREAD_TEST_NAME);
    AtomicInteger count = new AtomicInteger(20);
    ScheduledFuture<?> submitRunnable =
        executorService.scheduleWithFixedDelay(count::decrementAndGet, 10, 2, TimeUnit.SECONDS);
    assertThatCode(() -> submitRunnable.get(9, TimeUnit.SECONDS))
        .hasMessage("No element available");
    executionQueue.simulateWaitTime(999);
    assertThat(executionQueue.runStep()).isFalse();
    executionQueue.simulateWaitTime(1);
    assertThat(executionQueue.runStep()).isTrue();
    List<Runnable> terminatedRunners = executorService.shutdownNow();
    assertThat(terminatedRunners).hasSize(1);
    assertThat(count.get()).isEqualTo(19);
    executionQueue.simulateWaitTime(1_000_000);
    assertThat(executionQueue.runStep()).isFalse();
  }

  @Test
  void newContinuousExecution() {
    AtomicInteger exception = new AtomicInteger(0);
    AtomicInteger logger = new AtomicInteger(0);
    AtomicInteger count = new AtomicInteger(0);
    String threadName = "TestExecution";
    ContinuousExecution start =
        executionFactory
            .newContinuousExecution(
                threadName,
                () -> {
                  int i = count.incrementAndGet();
                  if (i == 3) {
                    throw new RuntimeException("Intended");
                  }
                })
            .logger(
                (s, t) -> {
                  assertThat(t).isInstanceOf(RuntimeException.class).hasMessage("Intended");
                  assertThat(s).isEqualTo("TestString");
                  logger.incrementAndGet();
                },
                "TestString")
            .onException(exception::incrementAndGet)
            .start();
    assertThat(start.isRunning()).isTrue();

    assertThat(count.get()).isEqualTo(0);
    TestExecutionQueue executionQueue = executionFactory.getExecutionQueue(threadName);
    executionQueue.runStep(2);
    assertThat(count.get()).isEqualTo(2);
    assertThat(exception.get()).isEqualTo(0);
    assertThat(logger.get()).isEqualTo(0);
    executionQueue.runStep();
    assertThat(count.get()).isEqualTo(3);
    assertThat(exception.get()).isEqualTo(1);
    assertThat(logger.get()).isEqualTo(1);
    assertThat(start.isRunning()).isTrue();
    start.close();
    assertThat(start.isRunning()).isFalse();
  }

  @Test
  void newSharedPoolSingleThreaded() throws Exception {
    String name = "TestName";
    try (SharedPoolSingleThreadedExecution sharedPoolSingleThreadedExecution =
        executionFactory.newSharedPoolSingleThreaded(name)) {
      AtomicInteger count = new AtomicInteger(0);
      SharedPoolSingleThreadedExecution.SingleThreadExecutor executor =
          sharedPoolSingleThreadedExecution.createSingleThreadedExecutor();
      assertThat(count.get()).isEqualTo(0);
      Future<Integer> execute = executor.execute(count::incrementAndGet);
      TestExecutionQueue executionQueue = executionFactory.getExecutionQueue(name);
      executionQueue.runStep(3);
      assertThat(execute.get()).isEqualTo(1);
      assertThat(count.get()).isEqualTo(1);
      execute = executor.execute(count::incrementAndGet);
      assertThat(execute.isDone()).isFalse();
      execute = executor.execute(count::incrementAndGet);
      assertThat(execute.isDone()).isFalse();
      executionQueue.runStep(3);
      assertThat(execute.get()).isEqualTo(3);
      assertThat(count.get()).isEqualTo(3);
    }
  }
}
