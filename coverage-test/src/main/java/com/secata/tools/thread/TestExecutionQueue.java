package com.secata.tools.thread;

import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.coverage.ThrowingRunnable;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * Holds all information about a specific execution queue for management in tests.
 *
 * <p>Every execution element is managed as a step by the {@link TestExecutionQueue#runStep} method
 * which executes one or more steps in the exposes executors.
 *
 * <p>Schedules are simulated using the {@link TestExecutionQueue#simulateWaitTime}, however in
 * order to execute, {@link TestExecutionQueue#runStep} must be called to activate it.
 */
public class TestExecutionQueue {

  private final DelayQueue<Executable<?>> executables;
  private boolean shutDownCalled;
  private int stepsAfterTimeToWait;
  private long timeToWaitInMillis;

  TestExecutionQueue() {
    this.executables = new DelayQueue<>();
  }

  /**
   * Runs a single step in the exposes executors.
   *
   * @return true if this succeeded, i.e. an execution occurred
   */
  public boolean runStep() {
    return runStep(1) == 1;
  }

  /**
   * Runs multiple steps in the exposes executors, execution stops if there are no more elements or
   * count is zero.
   *
   * @param count the number of times to run a single step.
   * @return the amount of executions - a number from 0 to count
   */
  public int runStep(int count) {
    if (count == 0 || shutDownCalled) {
      return 0;
    }
    Executable<?> nextElement = executables.peek();
    if (nextElement == null || nextElement.remainingDelayInMillis > 0) {
      return 0;
    }
    Executable<?> executable = executables.poll();
    executable.singleStep();
    return runStep(count - 1) + 1;
  }

  /**
   * Plans to run steps after a certain time has expired. This is useful for testing schedules in
   * planned executions, that is testing of {@link ScheduledExecutorService}.
   *
   * @param count the amount of steps (in run steps)
   * @param timeToWaitInMillis the amount of
   */
  public void runStepAfterTime(int count, long timeToWaitInMillis) {
    this.stepsAfterTimeToWait = count;
    this.timeToWaitInMillis = timeToWaitInMillis;
  }

  /**
   * Simulate time passing, waiting for the number of milliseconds.
   *
   * @param milliseconds the amount of milliseconds to wait
   */
  public void simulateWaitTime(long milliseconds) {
    for (Executable<?> executable : executables) {
      executable.simulateElementWaitTime(milliseconds);
    }
    timeToWaitInMillis -= milliseconds;
    if (timeToWaitInMillis <= 0) {
      runStep(stepsAfterTimeToWait);
      stepsAfterTimeToWait = 0;
    }
  }

  private void addElement(Executable<?> executable) {
    executables.add(executable);
  }

  private void shutDownCalled() {
    shutDownCalled = true;
  }

  private List<Runnable> shutDownCalledGetRunnable() {
    return executables.stream()
        .map(
            e -> {
              e.testScheduledFuture.cancel(true);
              return (Runnable) () -> ExceptionConverter.call(e.callable, "Inner error");
            })
        .toList();
  }

  /**
   * Checks whether shutdown has been called on this executor.
   *
   * @return true if shutdown
   */
  public boolean isExecutorShutDown() {
    return shutDownCalled;
  }

  ExecutorService newTestExecutorService() {
    return new TestExecutorService(this);
  }

  ScheduledExecutorService newTestScheduledExecutorService() {
    return new TestScheduledExecutorService(this);
  }

  ContinuousExecution.Builder newTestContinuousExecution(ThrowingRunnable runnable) {
    return new TestContinuousExecution(runnable);
  }

  private final class Executable<T> implements Delayed {

    private final Callable<T> callable;
    private final TestScheduledFuture testScheduledFuture;
    private long remainingDelayInMillis;
    private T result;
    private boolean done;

    private Executable(Runnable runnable, long remainingDelayInMillis, T result) {
      this(Executors.callable(runnable, result), remainingDelayInMillis);
    }

    private Executable(Callable<T> callable, long remainingDelayInMillis) {
      this.callable = callable;
      this.remainingDelayInMillis = remainingDelayInMillis;
      this.testScheduledFuture = new TestScheduledFuture(this);
    }

    private void singleStep() {
      done = true;
      result = ExceptionConverter.call(callable, "Exception during the test of the callback");
    }

    private ScheduledFuture<T> getFuture() {
      return testScheduledFuture;
    }

    private void simulateElementWaitTime(long milliseconds) {
      remainingDelayInMillis = Math.max(0, remainingDelayInMillis - milliseconds);
    }

    @Override
    public long getDelay(TimeUnit unit) {
      return testScheduledFuture.getDelay(unit);
    }

    @Override
    public int compareTo(Delayed o) {
      long myDelay = getDelay(TimeUnit.MILLISECONDS);
      long otherDelay = o.getDelay(TimeUnit.MILLISECONDS);
      return Long.compare(myDelay, otherDelay);
    }

    private class TestScheduledFuture implements ScheduledFuture<T> {

      private final Executable<T> executable;
      private boolean shutDownCalled;

      private TestScheduledFuture(Executable<T> executable) {
        this.executable = executable;
      }

      @Override
      public int compareTo(Delayed o) {
        return Long.compare(
            executable.remainingDelayInMillis,
            ((Executable.TestScheduledFuture) o).executable.remainingDelayInMillis);
      }

      @Override
      public long getDelay(TimeUnit unit) {
        return unit.convert(remainingDelayInMillis, TimeUnit.MILLISECONDS);
      }

      @Override
      public boolean cancel(boolean mayInterruptIfRunning) {
        shutDownCalled = true;
        return false;
      }

      @Override
      public boolean isCancelled() {
        return shutDownCalled;
      }

      @Override
      public boolean isDone() {
        return executable.done;
      }

      @Override
      public T get() {
        return result;
      }

      @Override
      public T get(long timeout, TimeUnit unit) throws TimeoutException {
        simulateWaitTime(Duration.of(timeout, unit.toChronoUnit()).toMillis());
        T result = get();
        if (result != null) {
          return result;
        } else {
          throw new TimeoutException("No element available");
        }
      }
    }
  }

  private abstract class AbstractTestExecutorService implements ExecutorService {

    private final TestExecutionQueue executionQueue;

    private AbstractTestExecutorService(TestExecutionQueue queue) {
      this.executionQueue = queue;
    }

    @Override
    public void shutdown() {
      executionQueue.shutDownCalled();
    }

    @Override
    public List<Runnable> shutdownNow() {
      shutdown();
      return executionQueue.shutDownCalledGetRunnable();
    }

    @Override
    public boolean isShutdown() {
      return executionQueue.isExecutorShutDown();
    }

    @Override
    public boolean isTerminated() {
      return executionQueue.isExecutorShutDown();
    }

    @Override
    public boolean awaitTermination(long timeout, TimeUnit unit) {
      return false;
    }

    @Override
    public <S> ScheduledFuture<S> submit(Callable<S> task) {
      Executable<S> executable = new Executable<>(task, 0);
      addElement(executable);
      return executable.getFuture();
    }

    @Override
    public <T> ScheduledFuture<T> submit(Runnable task, T result) {
      Executable<T> executable = new Executable<>(task, 0, result);
      addElement(executable);
      return executable.getFuture();
    }

    @Override
    public ScheduledFuture<?> submit(Runnable task) {
      return submit(task, null);
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) {
      return invokeAll(tasks, 0, TimeUnit.SECONDS);
    }

    @Override
    public <T> List<Future<T>> invokeAll(
        Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) {
      return planAndExecute(tasks, timeout, unit)
          .map(executable -> (Future<T>) executable.getFuture())
          .toList();
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> tasks) {
      return findFirst(planAndExecute(tasks, 0, TimeUnit.SECONDS));
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) {
      return findFirst(planAndExecute(tasks, timeout, unit));
    }

    private <T> Stream<Executable<T>> planAndExecute(
        Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) {
      List<Executable<T>> list = new ArrayList<>();
      for (Callable<T> callable : tasks) {
        Executable<T> executable = new Executable<>(callable, 0);
        list.add(executable);
        addElement(executable);
      }
      simulateWaitTime(unit.toMillis(timeout));
      return list.stream();
    }

    private <T> T findFirst(Stream<Executable<T>> tasks) {
      return tasks
          .map(Executable::getFuture)
          .map(future -> ExceptionConverter.call(future::get, "Internal error"))
          .filter(Objects::nonNull)
          .findFirst()
          .orElse(null);
    }

    @Override
    public void execute(Runnable command) {
      addElement(new Executable<>(command, 0, null));
    }
  }

  private final class TestExecutorService extends AbstractTestExecutorService {

    private TestExecutorService(TestExecutionQueue queue) {
      super(queue);
    }
  }

  private final class TestScheduledExecutorService extends AbstractTestExecutorService
      implements ScheduledExecutorService {

    private TestScheduledExecutorService(TestExecutionQueue queue) {
      super(queue);
    }

    @Override
    public ScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit) {
      return submitWithTimeDelay(command, delay, null, unit);
    }

    @Override
    public <V> ScheduledFuture<V> schedule(Callable<V> callable, long delay, TimeUnit unit) {
      return submitWithTimeDelay(callable, delay, unit);
    }

    @Override
    public ScheduledFuture<?> scheduleAtFixedRate(
        Runnable command, long initialDelay, long period, TimeUnit unit) {
      return submitWithTimeDelay(command, initialDelay, period, unit);
    }

    @Override
    public ScheduledFuture<?> scheduleWithFixedDelay(
        Runnable command, long initialDelay, long delay, TimeUnit unit) {
      return submitWithTimeDelay(command, initialDelay, delay, unit);
    }

    private <T> ScheduledFuture<T> submitWithTimeDelay(
        Callable<T> task, long delay, TimeUnit unit) {
      Executable<T> executable =
          new Executable<>(task, Duration.of(delay, unit.toChronoUnit()).toMillis());
      return addExecutable(executable);
    }

    private <T> ScheduledFuture<T> submitWithTimeDelay(
        Runnable command, long initialDelay, Long delay, TimeUnit unit) {
      Executable<T> executable = createExecutable(command, initialDelay, delay, unit);
      return addExecutable(executable);
    }

    private <T> Executable<T> createExecutable(
        Runnable command, long initialDelay, Long delay, TimeUnit unit) {
      Runnable effectiveCommand = command;
      if (delay != null) {
        effectiveCommand =
            () -> {
              command.run();
              addElement(createExecutable(command, initialDelay, delay, unit));
            };
      }
      return new Executable<>(
          effectiveCommand, Duration.of(initialDelay, unit.toChronoUnit()).toMillis(), null);
    }

    private <T> ScheduledFuture<T> addExecutable(Executable<T> executable) {
      addElement(executable);
      return executable.getFuture();
    }
  }

  private class TestContinuousExecution implements ContinuousExecution.Builder {

    private final ThrowingRunnable throwingRunnable;
    private Runnable exception = FunctionUtility.noOpRunnable();
    private Consumer<Throwable> logger = FunctionUtility.noOpConsumer();

    private TestContinuousExecution(ThrowingRunnable throwingRunnable) {
      this.throwingRunnable = throwingRunnable;
    }

    @Override
    public ContinuousExecution.Builder onException(Runnable runnable) {
      this.exception = runnable;
      return this;
    }

    @Override
    public ContinuousExecution.Builder logger(Consumer<Throwable> logger) {
      this.logger = logger;
      return this;
    }

    @Override
    public ContinuousExecution.Builder logger(
        BiConsumer<String, Throwable> biConsumerLogger, String fixedHeaderString) {
      return logger(t -> biConsumerLogger.accept(fixedHeaderString, t));
    }

    @Override
    public ContinuousExecution start() {
      createRepeatableRunnable();
      return new ContinuousExecution() {
        @Override
        public boolean isRunning() {
          return !shutDownCalled;
        }

        @Override
        public void close() {
          shutDownCalled = true;
        }
      };
    }

    private void createRepeatableRunnable() {
      addElement(new Executable<>(this::execute, 0, null));
    }

    /** Executes a single step and adds next element as the recursive step. */
    private void execute() {
      executeSingleStep();
      createRepeatableRunnable();
    }

    private void executeSingleStep() {
      try {
        throwingRunnable.run();
      } catch (Throwable e) {
        logger.accept(e);
        exception.run();
      }
    }
  }
}
