package com.secata.tools.thread;

import com.secata.tools.coverage.ThrowingRunnable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Test implementation of ExecutionFactory allowing single stepping each execution. Each queue can
 * be obtained by calling {@link #getExecutionQueue} - which returns a {@link TestExecutionQueue},
 * which manages regular and scheduled execution.
 *
 * <p>An exception to the queue management are the {@link ScheduledExecutorService#invokeAll} and
 * {@link ScheduledExecutorService#invokeAny}. Invoke are immediately executed with a runStep.
 */
public final class ExecutionFactoryFake implements ExecutionFactory {

  private final Map<String, TestExecutionQueue> executables = new HashMap<>();

  /**
   * Creates a test implementation of ExecutionFactory allowing single stepping each execution.
   *
   * @return the new factory
   */
  public static ExecutionFactoryFake create() {
    return new ExecutionFactoryFake();
  }

  /**
   * Gets the names of every registered queue in this factory.
   *
   * @return the collection of queue names
   */
  public Collection<String> getQueueNames() {
    return Collections.unmodifiableCollection(executables.keySet());
  }

  @Override
  public ExecutorService newSingleThreadExecutor(String name) {
    return createQueue(name).newTestExecutorService();
  }

  @Override
  public ExecutorService newCachedThreadPool(String name) {
    return createQueue(name).newTestExecutorService();
  }

  @Override
  public ScheduledExecutorService newScheduledThreadPool(String name, int corePoolSize) {
    return createQueue(name).newTestScheduledExecutorService();
  }

  @Override
  public ScheduledExecutorService newSingleThreadScheduledExecutor(String name) {
    return createQueue(name).newTestScheduledExecutorService();
  }

  @Override
  public ContinuousExecution.Builder newContinuousExecution(
      String name, ThrowingRunnable runnable) {
    return createQueue(name).newTestContinuousExecution(runnable);
  }

  private TestExecutionQueue createQueue(String name) {
    executables.putIfAbsent(name, new TestExecutionQueue());
    return executables.get(name);
  }

  @Override
  public SharedPoolSingleThreadedExecution newSharedPoolSingleThreaded(String name) {
    return new SharedPoolSingleThreadedExecution(newCachedThreadPool(name));
  }

  /**
   * Gets the test spy for the underlying queue.
   *
   * @param name the unique name of the queue.
   * @return the test execution queue.
   */
  public TestExecutionQueue getExecutionQueue(String name) {
    return executables.get(name);
  }
}
