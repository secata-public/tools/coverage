package com.secata.tools.coverage;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/** Utility class for getting no-op functions. */
public final class FunctionUtility {

  private FunctionUtility() {}

  /**
   * Create a function that always returns null.
   *
   * @param <T> from-type.
   * @param <S> to-type.
   * @return a function that returns null.
   */
  public static <T, S> Function<T, S> nullFunction() {
    return t -> null;
  }

  /**
   * Create a bi-function that always returns null.
   *
   * @param <T> first from-type.
   * @param <S> second from-type.
   * @param <U> to-type.
   * @return a bi-function that returns null.
   */
  public static <T, S, U> BiFunction<T, S, U> nullBiFunction() {
    return (t, s) -> null;
  }

  /**
   * Create consumer that ignores the input.
   *
   * @param <T> first from-type.
   * @param <S> second from-type.
   * @return a no-op bi-consumer.
   */
  public static <T, S> BiConsumer<T, S> noOpBiConsumer() {
    return (t, s) -> {};
  }

  /**
   * Create consumer that ignores the input.
   *
   * @param <T> from-type.
   * @return a no-op consumer.
   */
  public static <T> Consumer<T> noOpConsumer() {
    return t -> {};
  }

  /**
   * Create runnable that does nothing.
   *
   * @return a no-op runnable.
   */
  public static Runnable noOpRunnable() {
    return () -> {};
  }

  /**
   * Create supplier that always returns null.
   *
   * @param <T> to-type.
   * @return a supplier returning null.
   */
  public static <T> Supplier<T> nullSupplier() {
    return () -> null;
  }
}
