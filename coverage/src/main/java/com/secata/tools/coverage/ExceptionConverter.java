package com.secata.tools.coverage;

import java.util.concurrent.Callable;

/**
 * This class converts typed exceptions to runtime exceptions. Use with care, only typed exceptions
 * should be converted.
 */
public final class ExceptionConverter {

  private ExceptionConverter() {}

  /**
   * Use this method to catch any exception thrown in callable and rethrow as a RuntimeException.
   * This effectively removes the checked exceptions.
   *
   * @param callable the method to execute
   * @param <T> the return type
   * @return the computed value
   */
  public static <T> T call(Callable<T> callable) {
    try {
      return callable.call();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Use this method to catch any exception thrown in callable and rethrow as a RuntimeException.
   * This effectively removes the checked exceptions.
   *
   * @param callable the method to execute
   * @param message the error message if there is an exception
   * @param <T> the return type
   * @return the computed value
   */
  public static <T> T call(Callable<T> callable, String message) {
    try {
      return callable.call();
    } catch (Exception e) {
      throw new RuntimeException(message, e);
    }
  }

  /**
   * Use this method to catch any exception thrown in runnable and rethrow as a RuntimeException.
   * This effectively removes the checked exceptions.
   *
   * @param runnable the method to execute
   */
  public static void run(ThrowingRunnable runnable) {
    try {
      runnable.run();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Use this method to catch any exception thrown in runnable and rethrow as a RuntimeException.
   * This effectively removes the checked exceptions.
   *
   * @param runnable the method to execute
   * @param message the error message if there is an exception
   */
  public static void run(ThrowingRunnable runnable, String message) {
    try {
      runnable.run();
    } catch (Exception e) {
      throw new RuntimeException(message, e);
    }
  }
}
