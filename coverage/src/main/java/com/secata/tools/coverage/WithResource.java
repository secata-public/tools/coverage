package com.secata.tools.coverage;

import java.util.function.Supplier;

/**
 * This class converts typed exceptions to runtime exceptions. Use with care, only typed exceptions
 * should be converted.
 */
public final class WithResource {

  private WithResource() {}

  /**
   * Use this method to encapsulate a try-with-resources.
   *
   * @param supplier the supplier of the resource to be closed.
   * @param callable the method to execute.
   * @param <T> the type of closeable.
   */
  @SuppressWarnings("try")
  public static <T extends AutoCloseable> void accept(
      ThrowingSupplier<T> supplier, ThrowingConsumer<T> callable) {
    try (T autoCloseable = supplier.get()) {
      callable.accept(autoCloseable);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Use this method to encapsulate a try-with-resources.
   *
   * @param supplier the supplier of the resource to be closed.
   * @param callable the method to execute.
   * @param message the error message if there is an exception
   * @param <T> the type of closeable.
   */
  public static <T extends AutoCloseable> void accept(
      ThrowingSupplier<T> supplier, ThrowingConsumer<T> callable, String message) {
    accept(supplier, callable, () -> message);
  }

  /**
   * Use this method to encapsulate a try-with-resources.
   *
   * @param supplier the supplier of the resource to be closed.
   * @param callable the method to execute.
   * @param message how to produce the error message if there is an exception
   * @param <T> the type of closeable.
   */
  @SuppressWarnings("try")
  public static <T extends AutoCloseable> void accept(
      ThrowingSupplier<T> supplier, ThrowingConsumer<T> callable, Supplier<String> message) {
    try (T autoCloseable = supplier.get()) {
      callable.accept(autoCloseable);
    } catch (Exception e) {
      throw new RuntimeException(message.get(), e);
    }
  }

  /**
   * Use this method to encapsulate a try-with-resources.
   *
   * @param supplier the supplier of the resource to be closed.
   * @param callable the method to execute.
   * @param <T> the type of closeable.
   * @param <S> the the return type.
   * @return s if successful
   */
  @SuppressWarnings("try")
  public static <T extends AutoCloseable, S> S apply(
      ThrowingSupplier<T> supplier, ThrowingFunction<T, S> callable) {
    try (T autoCloseable = supplier.get()) {
      return callable.apply(autoCloseable);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Use this method to encapsulate a try-with-resources.
   *
   * @param supplier the supplier of the resource to be closed.
   * @param callable the method to execute.
   * @param message the error message if there is an exception
   * @param <T> the type of closeable.
   * @param <S> the the return type.
   * @return s if successful
   */
  public static <T extends AutoCloseable, S> S apply(
      ThrowingSupplier<T> supplier, ThrowingFunction<T, S> callable, String message) {
    return apply(supplier, callable, () -> message);
  }

  /**
   * Use this method to encapsulate a try-with-resources.
   *
   * @param supplier the supplier of the resource to be closed.
   * @param callable the method to execute.
   * @param message how to produce the error message if there is an exception
   * @param <T> the type of closeable.
   * @param <S> the the return type.
   * @return s if successful
   */
  @SuppressWarnings("try")
  public static <T extends AutoCloseable, S> S apply(
      ThrowingSupplier<T> supplier, ThrowingFunction<T, S> callable, Supplier<String> message) {
    try (T autoCloseable = supplier.get()) {
      return callable.apply(autoCloseable);
    } catch (Exception e) {
      throw new RuntimeException(message.get(), e);
    }
  }
}
