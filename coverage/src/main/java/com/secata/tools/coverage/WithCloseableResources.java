package com.secata.tools.coverage;

import java.util.stream.Stream;
import org.slf4j.LoggerFactory;

/** Implementation of an aggregate closeable. */
public interface WithCloseableResources extends AutoCloseable {

  @Override
  default void close() {
    resources().forEach(this::closeElement);
  }

  private void closeElement(AutoCloseable closeable) {
    ExceptionLogger.handle(
        LoggerFactory.getLogger(WithCloseableResources.class)::info,
        closeable::close,
        "Unable to close resource");
  }

  /**
   * Gets the underlying resources connected to this container.
   *
   * @return the stream of underlying resources
   */
  Stream<AutoCloseable> resources();
}
