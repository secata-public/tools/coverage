package com.secata.tools.coverage;

/**
 * Throwing version of {@link java.util.function.Consumer}.
 *
 * @param <T> the value being supplied.
 */
@FunctionalInterface
public interface ThrowingConsumer<T> {

  /**
   * Consumes the given value.
   *
   * @param input the input.
   * @throws Exception on error.
   */
  void accept(T input) throws Exception;
}
