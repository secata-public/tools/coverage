package com.secata.tools.coverage;

/**
 * A throwing version of {@link java.util.function.Function}.
 *
 * @param <T> the input
 * @param <R> the result of the function
 */
@FunctionalInterface
public interface ThrowingFunction<T, R> {

  /**
   * Applies the function.
   *
   * @param input the input of the function.
   * @return the result.
   * @throws Exception on error
   */
  R apply(T input) throws Exception;
}
