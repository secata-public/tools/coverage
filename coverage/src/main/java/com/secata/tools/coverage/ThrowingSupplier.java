package com.secata.tools.coverage;

/**
 * A throwing version of {@link java.util.function.Supplier}.
 *
 * @param <T> the type being supplied.
 */
@FunctionalInterface
public interface ThrowingSupplier<T> {

  /**
   * Supplies a value.
   *
   * @return a value.
   * @throws Exception on error.
   */
  T get() throws Exception;
}
