package com.secata.tools.coverage;

/**
 * An extended version of {@link Runnable} that allows exceptions to be thrown. This interface only
 * changes the signature.
 */
@FunctionalInterface
public interface ThrowingRunnable {

  /**
   * Performs the task of this runnable.
   *
   * @throws Exception on error
   */
  void run() throws Exception;
}
