package com.secata.tools.coverage;

import java.util.function.BiConsumer;

/** Utility class for logging throwables instead of throwing them. */
public final class ExceptionLogger {

  private ExceptionLogger() {}

  /**
   * Run the supplied runnable and log any throwable to the supplied logger.
   *
   * @param runnable the runnable to execute
   * @param message the message if the runnable fails
   * @param logger the logging mechanism if the runnable fails
   */
  public static void handle(
      BiConsumer<String, Throwable> logger, ThrowingRunnable runnable, String message) {
    try {
      runnable.run();
    } catch (Throwable t) {
      logger.accept(message, t);
    }
  }
}
