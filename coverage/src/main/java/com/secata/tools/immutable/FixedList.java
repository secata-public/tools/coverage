package com.secata.tools.immutable;

import com.google.errorprone.annotations.CheckReturnValue;
import com.google.errorprone.annotations.Immutable;
import com.google.errorprone.annotations.ImmutableTypeParameter;
import com.google.errorprone.annotations.InlineMe;
import java.util.AbstractCollection;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * An immutable and unmodifiable {@link List}.
 *
 * <p>The contents of data structure cannot change, as the list is backed by a fixed array.
 * Modifying methods from {@link Collection} will fail with {@link UnsupportedOperationException}.
 *
 * @param <T> Type of the list contents. Must be {@link Immutable}.
 */
@Immutable
@CheckReturnValue
public final class FixedList<@ImmutableTypeParameter T> extends AbstractCollection<T> {

  /** Empty fixed list constant for use with {@link #create}. */
  @SuppressWarnings("Immutable")
  private static final FixedList<?> EMPTY = new FixedList<>(new Object[0], 0, 0);

  /** Internal backing array of the contents. */
  @SuppressWarnings("Immutable")
  private final Object[] data;

  /**
   * The first index of {@link #data} to allow indexing into.
   *
   * <p>Invariant: Minimum {@code 0}, maximum {@code min(toIndex, data.length)}.
   */
  private final int startIndex;

  /**
   * The index after the last index of {@link #data} to allow indexing into.
   *
   * <p>Invariant: Minimum {@code max(0, startIndex)}, maximum {@code data.length}.
   */
  private final int toIndex;

  private FixedList(Object[] data, int startIndex, int toIndex) {
    this.data = data;
    this.startIndex = startIndex;
    this.toIndex = toIndex;
    subListRangeCheck(startIndex, toIndex);
    if (toIndex - 1 - startIndex >= data.length) {
      throw new IndexOutOfBoundsException(
          "lastElementIndex = " + (startIndex + toIndex) + " data.length = " + data.length);
    }
  }

  private void subListRangeCheck(int fromIndex, int toIndex) {
    if (fromIndex < 0) {
      throw new IndexOutOfBoundsException("fromIndex = " + fromIndex);
    }
    if (fromIndex > toIndex) {
      throw new IllegalArgumentException("fromIndex(" + fromIndex + ") > toIndex(" + toIndex + ")");
    }
  }

  /**
   * Creates an empty {@link FixedList}.
   *
   * @param <T> the type of elements
   * @return the updated {@link FixedList}. Never null.
   */
  @SuppressWarnings({"Immutable", "unchecked"})
  @CheckReturnValue
  public static <T> FixedList<T> create() {
    return (FixedList<T>) EMPTY;
  }

  /**
   * Creates a {@link FixedList} from an existing {@link Collection}.
   *
   * @param collection the collection to initialize from
   * @param <T> the type of elements
   * @return the updated {@link FixedList}. Never null.
   */
  @CheckReturnValue
  public static <T> FixedList<T> create(Collection<? extends T> collection) {
    return fromArray(collection.toArray());
  }

  /**
   * Creates a {@link FixedList} from a {@link Stream} which is assumed to have a finite size.
   *
   * @param stream the stream to initialize from
   * @param <T> the type of elements
   * @return the updated {@link FixedList}. Never null.
   */
  @CheckReturnValue
  public static <T> FixedList<T> create(Stream<? extends T> stream) {
    Object[] array = stream.toArray(Object[]::new);
    return fromArray(array);
  }

  @SuppressWarnings("Immutable")
  @CheckReturnValue
  private static <T> FixedList<T> fromArray(Object[] ts) {
    return new FixedList<>(ts, 0, ts.length);
  }

  /**
   * Creates a new {@link FixedList} containing every element in the existing list as well as the
   * new element.
   *
   * @param element the element to add
   * @return the updated {@link FixedList}. Never null.
   */
  @CheckReturnValue
  public FixedList<T> addElement(T element) {
    Object[] ts = Arrays.copyOfRange(data, startIndex, toIndex + 1);
    ts[size()] = element;
    return fromArray(ts);
  }

  /**
   * Creates a new {@link FixedList} containing every element in the existing list apart from the
   * supplied element. If the element does not exists, then this method is a No-Op.
   *
   * @param element the element to remove
   * @return the updated {@link FixedList}. Never null.
   */
  @CheckReturnValue
  public FixedList<T> removeElement(T element) {
    int index = indexOf(element);
    if (index == -1) {
      return this;
    }
    return removeElementAtIndex(index);
  }

  /**
   * Creates a new {@link FixedList} containing every element in the existing list apart from the
   * element at the supplied index.
   *
   * @param index the index of the element to remove
   * @return the updated {@link FixedList}. Never null.
   * @throws IndexOutOfBoundsException if given index is out of bounds.
   * @deprecated Deprecated to reduce naming conflicts. Use either {@link #removeElement(T)} or
   *     {@link #removeElementAtIndex(int)}.
   */
  @Deprecated
  @InlineMe(replacement = "this.removeElementAtIndex(index)")
  @CheckReturnValue
  public FixedList<T> removeElement(int index) {
    return removeElementAtIndex(index);
  }

  /**
   * Creates a new {@link FixedList} containing every element in the existing list apart from the
   * element at the supplied index.
   *
   * @param index the index of the element to remove
   * @return the updated {@link FixedList}. Never null.
   * @throws IndexOutOfBoundsException if given index is out of bounds.
   */
  @CheckReturnValue
  public FixedList<T> removeElementAtIndex(int index) {
    int srcIndex = index + startIndex;
    Objects.checkIndex(index, size());
    Object[] ts = new Object[size() - 1];
    System.arraycopy(data, startIndex, ts, 0, index);
    System.arraycopy(data, srcIndex + 1, ts, index, toIndex - srcIndex - 1);
    return fromArray(ts);
  }

  /**
   * Returns the index of the specified element. The lowest index will be used if the element occurs
   * multiple times.
   *
   * @param element the element to find in the list
   * @return the index, or -1 if not in the list
   */
  @CheckReturnValue
  public int indexOf(T element) {
    for (int i = startIndex; i < toIndex; i++) {
      if (Objects.equals(data[i], element)) {
        return i;
      }
    }
    return -1;
  }

  /**
   * Creates a new {@link FixedList} containing every element in the existing list as well as the
   * new elements.
   *
   * @param elements the elements to add
   * @return the updated {@link FixedList}. Never null.
   */
  @CheckReturnValue
  public FixedList<T> addElements(Collection<T> elements) {
    Object[] ts = Arrays.copyOfRange(data, startIndex, toIndex + elements.size());
    int index = size();
    for (T element : elements) {
      ts[index++] = element;
    }
    return fromArray(ts);
  }

  /**
   * Creates a new {@link FixedList} containing every element in the existing list except for the
   * one at index which is overwritten.
   *
   * @param index index to overwrite
   * @param element the element to update with
   * @return the updated {@link FixedList}. Never null.
   * @throws IndexOutOfBoundsException if given index is out of bounds.
   */
  @CheckReturnValue
  public FixedList<T> setElement(int index, T element) {
    Objects.checkIndex(index, size());
    Object[] ts = Arrays.copyOfRange(data, startIndex, toIndex);
    ts[index] = element;
    return fromArray(ts);
  }

  /**
   * Creates a sublist of this fixed list.
   *
   * <p>This is very efficient.
   *
   * @param from index to start from (inclusive)
   * @param to index to end at (exclusive)
   * @return the updated {@link FixedList}. Never null.
   * @throws IndexOutOfBoundsException if given index is out of bounds.
   */
  @CheckReturnValue
  public FixedList<T> subList(int from, int to) {
    return new FixedList<>(data, this.startIndex + from, this.startIndex + to);
  }

  /**
   * Get element at the given index.
   *
   * @param index index of element to lookup in list
   * @return the element
   */
  @SuppressWarnings("unchecked")
  @CheckReturnValue
  public T get(int index) {
    Objects.checkIndex(index, size());
    return (T) data[index + startIndex];
  }

  @Override
  @CheckReturnValue
  public Iterator<T> iterator() {
    return new Iterator<>() {
      int count = 0;

      @Override
      public boolean hasNext() {
        return count < size();
      }

      @Override
      public T next() {
        return get(count++);
      }
    };
  }

  @Override
  @CheckReturnValue
  public int size() {
    return toIndex - startIndex;
  }
}
