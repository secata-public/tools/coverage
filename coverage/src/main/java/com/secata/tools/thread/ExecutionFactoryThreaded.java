package com.secata.tools.thread;

import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.coverage.ThrowingRunnable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Factory for creating executors, make sure the naming is predictable. Users of this factory should
 * keep references for the executors to ensure that they are closed at the appropriate point in
 * time.
 */
public final class ExecutionFactoryThreaded implements ExecutionFactory {

  /**
   * Creates a new concrete factory intended to be used in production with threaded implementations.
   *
   * @return the new factory
   */
  public static ExecutionFactoryThreaded create() {
    return new ExecutionFactoryThreaded();
  }

  /**
   * Creates a new executor service with a single thread pool.
   *
   * @param name the name of this thread pool
   * @return the executor service
   */
  @Override
  public ExecutorService newSingleThreadExecutor(String name) {
    ExecutorService executorService =
        Executors.newSingleThreadExecutor(new NamedThreadFactory(name));
    return executorService;
  }

  /**
   * Creates a new executor service with a cached thread pool.
   *
   * @param name the name of this thread pool
   * @return the executor service
   */
  @Override
  public ExecutorService newCachedThreadPool(String name) {
    ExecutorService executorService = Executors.newCachedThreadPool(new NamedThreadFactory(name));
    return executorService;
  }

  /**
   * Creates a new scheduled executor service with multiple threads backing it.
   *
   * @param name the name of this thread pool
   * @param corePoolSize size of the core pool
   * @return the scheduled executor service
   */
  @Override
  public ScheduledExecutorService newScheduledThreadPool(String name, int corePoolSize) {
    ScheduledExecutorService scheduledExecutorService =
        Executors.newScheduledThreadPool(corePoolSize, new NamedThreadFactory(name));
    return scheduledExecutorService;
  }

  /**
   * Creates a new scheduled executor service with a single thread backing it.
   *
   * @param name the name of this thread pool
   * @return the scheduled executor service
   */
  @Override
  public ScheduledExecutorService newSingleThreadScheduledExecutor(String name) {
    ScheduledExecutorService scheduledExecutorService =
        Executors.newSingleThreadScheduledExecutor(new NamedThreadFactory(name));
    return scheduledExecutorService;
  }

  @Override
  public ContinuousExecution.Builder newContinuousExecution(
      String name, ThrowingRunnable runnable) {
    return new ContinuousExecutionBuilder(runnable, name);
  }

  @Override
  public SharedPoolSingleThreadedExecution newSharedPoolSingleThreaded(String name) {
    SharedPoolSingleThreadedExecution singleThreadedExecution =
        SharedPoolSingleThreadedExecution.named(name);
    return singleThreadedExecution;
  }

  private static class NamedThreadFactory implements ThreadFactory {

    private static final AtomicInteger threadNumber = new AtomicInteger(1);

    private final String name;

    public NamedThreadFactory(String name) {
      this.name = name;
    }

    @Override
    public Thread newThread(Runnable r) {
      return new Thread(r, name + "-" + threadNumber.getAndIncrement());
    }
  }

  private static final class ContinuousExecutionBuilder implements ContinuousExecution.Builder {

    private static final Consumer<Throwable> defaultLogger = new SimpleDebugLogger();

    private final ThrowingRunnable runnable;
    private final String name;
    private Runnable onException = FunctionUtility.noOpRunnable();
    private Consumer<Throwable> logger = defaultLogger;

    private ContinuousExecutionBuilder(ThrowingRunnable runnable, String name) {
      this.runnable = runnable;
      this.name = name;
    }

    @Override
    public ContinuousExecution.Builder onException(Runnable runnable) {
      this.onException = runnable;
      return this;
    }

    @Override
    public ContinuousExecution.Builder logger(Consumer<Throwable> logger) {
      this.logger = logger;
      return this;
    }

    @Override
    public ContinuousExecution.Builder logger(
        BiConsumer<String, Throwable> logger, String fixedHeaderString) {
      return logger(e -> logger.accept(fixedHeaderString, e));
    }

    @Override
    public ContinuousExecution start() {
      ThreadedLoop threadedLoop =
          ThreadedLoop.create(runnable, name).logger(logger).onException(onException).start();
      return threadedLoop;
    }
  }
}
