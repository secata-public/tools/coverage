package com.secata.tools.thread;

import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.coverage.ThrowingRunnable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/** Runs a thread in a loop, useful as a tested mechanism instead of rwa threads. */
public final class ThreadedLoop implements ContinuousExecution {

  private static final AtomicInteger threadNumber = new AtomicInteger(1);

  private final Consumer<Throwable> logger;
  private final ThrowingRunnable runnable;
  private final Runnable onThrowable;
  private final Thread thread;
  private boolean running;

  private ThreadedLoop(
      ThrowingRunnable runnable,
      Runnable onThrowable,
      String threadName,
      Consumer<Throwable> logger) {
    this.runnable = runnable;
    this.onThrowable = onThrowable;
    this.thread = new Thread(this::run, threadName + "-" + threadNumber.getAndIncrement());
    this.logger = logger;
    this.running = true;
  }

  /**
   * Creates a new threaded loop calling the provided runnable repeatedly until the stop method is
   * invoked.
   *
   * @param runnable the runnable to execute over and over
   * @param name of the resulting thread
   * @return this builder for future configuration
   */
  public static ThreadedLoop.Builder create(ThrowingRunnable runnable, String name) {
    return new Builder(runnable, name);
  }

  void run() {
    while (running) {
      try {
        runnable.run();
      } catch (Throwable t) {
        logger.accept(t);
        onThrowable.run();
      }
    }
  }

  @Override
  public boolean isRunning() {
    return running;
  }

  @Override
  public void close() {
    running = false;
    // In case the thread is waiting on a shared object
    thread.interrupt();
  }

  void join(long millis) throws InterruptedException {
    thread.join(millis);
  }

  /** Should be inlined in the future, is kept for backwards compatibility. */
  public void stop() {
    close();
  }

  /** Builder class for threaded loop. */
  public static class Builder {

    private static final SimpleDebugLogger defaultLogger = new SimpleDebugLogger();

    private final ThrowingRunnable runnable;
    private final String name;
    private Runnable onException = FunctionUtility.noOpRunnable();
    private Consumer<Throwable> logger = defaultLogger;

    private Builder(ThrowingRunnable runnable, String name) {
      this.runnable = runnable;
      this.name = name;
    }

    /**
     * Adds exception handler for this threaded loop. Default is just to ignore it.
     *
     * @param runnable is called whenever there is an exception raised in the runnable
     * @return this builder for future configuration
     */
    public Builder onException(Runnable runnable) {
      this.onException = runnable;
      return this;
    }

    /**
     * Adds a logger to the threaded loop, users of these loops must supply a logger to see errors
     * in console. A generic logger is provided but ought to be changed.
     *
     * @param logger the logging mechanism if the runnable fails
     * @return this builder for future configuration
     */
    public Builder logger(Consumer<Throwable> logger) {
      this.logger = logger;
      return this;
    }

    /**
     * Adds a logger to the threaded loop, users of these loops must supply a logger to see errors
     * in console. A generic logger is provided but ought to be changed. This logger mechanism plays
     * well with slf4j.
     *
     * @param logger the logging mechanism if the runnable fails
     * @param fixedHeaderString the fixed header of the logger
     * @return this builder for future configuration
     */
    public Builder logger(BiConsumer<String, Throwable> logger, String fixedHeaderString) {
      return logger(e -> logger.accept(fixedHeaderString, e));
    }

    /**
     * Starts a new threaded loop calling the provided runnable repeatedly until the stop method is
     * invoked.
     *
     * @return the created thread loop - should be used to terminate this
     */
    public ThreadedLoop start() {
      ThreadedLoop threadedLoop = new ThreadedLoop(runnable, onException, name, logger);
      threadedLoop.thread.start();
      return threadedLoop;
    }
  }
}
