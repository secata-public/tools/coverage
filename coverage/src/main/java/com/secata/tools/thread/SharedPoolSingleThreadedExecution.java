package com.secata.tools.thread;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Allows executing work on a shared pool of threads. Each executor will have their work executed in
 * serial.
 */
public final class SharedPoolSingleThreadedExecution implements AutoCloseable {

  private static final AtomicInteger threadNumber = new AtomicInteger(1);
  private final ExecutorService executor;

  SharedPoolSingleThreadedExecution(ExecutorService executor) {
    this.executor = executor;
  }

  private SharedPoolSingleThreadedExecution(int minThreads, String name) {
    this(
        new ThreadPoolExecutor(
            minThreads,
            Integer.MAX_VALUE,
            60L,
            TimeUnit.SECONDS,
            new SynchronousQueue<>(),
            r -> new Thread(r, "SPST-" + name + "-" + threadNumber.getAndIncrement())));
  }

  /**
   * Creates a SharedPoolSingleThreadedExecution given the name, allocates an underlying
   * ThreadPoolExecutor.
   *
   * @param name the name of this shared pool
   * @return the created SharedPoolSingleThreadedExecution
   */
  public static SharedPoolSingleThreadedExecution named(String name) {
    return new SharedPoolSingleThreadedExecution(0, name);
  }

  /**
   * Creates a single threaded executor.
   *
   * @return the created executor
   */
  public SingleThreadExecutor createSingleThreadedExecutor() {
    return new SingleThreadExecutor(executor);
  }

  @Override
  public void close() {
    executor.shutdownNow();
  }

  /**
   * Defines an interface for a serial execution. Equivalent to a single threaded execution, however
   * here we share a common thread pool in order to minimize the total number of threads in the
   * system.
   */
  public static final class SingleThreadExecutor implements Executor {

    private final Queue<Work<?>> queue;
    private final ExecutorService executor;
    private boolean running;

    SingleThreadExecutor(ExecutorService executor) {
      this.executor = executor;
      this.queue = new ArrayDeque<>();
    }

    @Override
    @SuppressWarnings("FutureReturnValueIgnored")
    public void execute(Runnable command) {
      execute(
          () -> {
            command.run();
            return null;
          });
    }

    /**
     * Submits a single task to the execution queue. If the executor has active work, then the work
     * is added to pending, otherwise it will be directly added to the executor.
     *
     * @param task the work to do
     * @param <ResultT> the resulting type of the work
     * @return the (uncompleted) result of the added work
     */
    public synchronized <ResultT> Future<ResultT> execute(Callable<ResultT> task) {
      if (isAlive()) {
        Work<ResultT> work = new Work<>(task, this::checkMoreWork);
        if (!running) {
          running = true;
          work.start(executor);
        } else {
          queue.add(work);
        }
        return work.asFuture();
      } else {
        return CompletableFuture.failedFuture(new RuntimeException("Executor is closed"));
      }
    }

    /**
     * Check for more work for a given executor. This call has the implicit assumption that this
     * executor has running = true.
     */
    private synchronized void checkMoreWork() {
      if (!queue.isEmpty()) {
        Work<?> work = queue.poll();
        work.start(executor);
      } else {
        running = false;
      }
    }

    boolean isAlive() {
      return !executor.isShutdown();
    }
  }

  static final class Work<ResultT> {

    private final Callable<ResultT> callable;
    private final Runnable checkForMoreWork;
    private final CompletableFuture<ResultT> future;

    Work(Callable<ResultT> runnable, Runnable checkForMoreWork) {
      this.callable = runnable;
      this.checkForMoreWork = checkForMoreWork;
      this.future = new CompletableFuture<>();
    }

    void start(Executor executor) {
      executor.execute(
          () -> {
            try {
              ResultT call = callable.call();
              future.complete(call);
            } catch (Throwable throwable) {
              future.completeExceptionally(throwable);
            } finally {
              checkForMoreWork.run();
            }
          });
    }

    private Future<ResultT> asFuture() {
      return future;
    }
  }
}
