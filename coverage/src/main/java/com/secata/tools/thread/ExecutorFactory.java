package com.secata.tools.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

/** Factory for creating executors, make sure the naming is predictable. */
public final class ExecutorFactory {

  private static final ExecutionFactoryThreaded EXECUTION_FACTORY_THREADED =
      ExecutionFactoryThreaded.create();

  private ExecutorFactory() {}

  /**
   * Creates a new executor service with a single thread pool.
   *
   * @param name the name of this thread pool
   * @return the executor service
   */
  public static ExecutorService newSingle(String name) {
    return EXECUTION_FACTORY_THREADED.newSingleThreadExecutor(name);
  }

  /**
   * Creates a new executor service with a cached thread pool.
   *
   * @param name the name of this thread pool
   * @return the executor service
   */
  public static ExecutorService newCached(String name) {
    return EXECUTION_FACTORY_THREADED.newCachedThreadPool(name);
  }

  /**
   * Creates a new scheduled executor service with multiple threads backing it.
   *
   * @param name the name of this thread pool
   * @param corePoolSize size of the core pool
   * @return the scheduled executor service
   */
  public static ScheduledExecutorService newScheduled(String name, int corePoolSize) {
    return EXECUTION_FACTORY_THREADED.newScheduledThreadPool(name, corePoolSize);
  }

  /**
   * Creates a new scheduled executor service with a single thread backing it.
   *
   * @param name the name of this thread pool
   * @return the scheduled executor service
   */
  public static ScheduledExecutorService newScheduledSingleThread(String name) {
    return EXECUTION_FACTORY_THREADED.newSingleThreadScheduledExecutor(name);
  }
}
