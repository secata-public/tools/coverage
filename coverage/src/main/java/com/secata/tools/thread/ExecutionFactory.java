package com.secata.tools.thread;

import com.secata.tools.coverage.ThrowingRunnable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Abstract factory for creating threads and executors. This factory allows replacing
 * production/threaded variant with test doubles implementation. This implementation supports named
 * threads to make traceability during runtime easy.
 */
public interface ExecutionFactory {

  /**
   * Creates a new executor service with a single thread pool.
   *
   * @param name the prefix name of the threads in this pool
   * @return the executor service
   */
  ExecutorService newSingleThreadExecutor(String name);

  /**
   * Creates a new executor service with a cached thread pool.
   *
   * @param name the prefix name of the threads in this pool
   * @return the executor service
   */
  ExecutorService newCachedThreadPool(String name);

  /**
   * Creates a new scheduled executor service with multiple threads backing it.
   *
   * @param name the prefix name of the threads in this pool
   * @param corePoolSize size of the core thread pool
   * @return the scheduled executor service
   */
  ScheduledExecutorService newScheduledThreadPool(String name, int corePoolSize);

  /**
   * Creates a new scheduled executor service with a single thread backing it.
   *
   * @param name the prefix name of the threads in this pool
   * @return the scheduled executor service
   */
  ScheduledExecutorService newSingleThreadScheduledExecutor(String name);

  /**
   * Builds a new threaded loop, this method returns a builder, call {@link
   * ContinuousExecution.Builder#start()} to start the thread.
   *
   * @param name the prefix name of the threads in this pool
   * @param runnable the runnable which will be executed in every loop.
   * @return a builder for a {@link ThreadedLoop}
   */
  ContinuousExecution.Builder newContinuousExecution(String name, ThrowingRunnable runnable);

  /**
   * Creates a new SharedPoolSingleThreadedExecution that allows executing work on a shared pool of
   * threads. Each executor will have their work executed in serial.
   *
   * @param name the infix name of the threads in this pool, the name starts with SPST-name-count
   * @return a new shared thread pool
   */
  SharedPoolSingleThreadedExecution newSharedPoolSingleThreaded(String name);
}
