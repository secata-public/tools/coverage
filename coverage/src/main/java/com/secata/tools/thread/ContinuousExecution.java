package com.secata.tools.thread;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Runs work as a runnable in a loop. This is intended to wrap a thread that runs continuously a
 * loop. This interface allows testing of executing this work without actual threading.
 */
public interface ContinuousExecution extends AutoCloseable {

  /**
   * Returns if this loop should run or if it has been stopped.
   *
   * @return true if the underlying thread is intended to keep on running
   */
  boolean isRunning();

  /**
   * Signal that the next iteration should not occur and stops the thread and interrupt the
   * underlying thread.
   */
  @Override
  void close();

  /** Builder for creating new ContinuousExecution implementations. */
  interface Builder {

    /**
     * Adds exception handler for this threaded loop. Default is just to ignore it.
     *
     * @param runnable is called whenever there is an exception raised in the runnable
     * @return this builder for future configuration
     */
    Builder onException(Runnable runnable);

    /**
     * Adds a logger to the threaded loop, users of these loops must supply a logger to see errors
     * in console. A generic logger is provided but ought to be changed.
     *
     * @param logger the logging mechanism if the runnable fails
     * @return this builder for future configuration
     */
    Builder logger(Consumer<Throwable> logger);

    /**
     * Adds a logger to the threaded loop, users of these loops must supply a logger to see errors
     * in console. A generic logger is provided but ought to be changed. This logger mechanism plays
     * well with slf4j.
     *
     * @param logger the logging mechanism if the runnable fails
     * @param fixedHeaderString the fixed header of the logger
     * @return this builder for future configuration
     */
    Builder logger(BiConsumer<String, Throwable> logger, String fixedHeaderString);

    /**
     * Starts a new threaded loop calling the provided runnable repeatedly until the stop method is
     * invoked.
     *
     * @return the created thread loop - should be used to terminate this
     */
    ContinuousExecution start();
  }
}
