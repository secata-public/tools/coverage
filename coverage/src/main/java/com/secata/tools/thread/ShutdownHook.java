package com.secata.tools.thread;

import com.secata.tools.coverage.ExceptionLogger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class for registering application wide shutdown hooks. This is closely linked to the Java
 * runtime, so use this for hooking into the runtime shutdown, not for arbitrary objects that may
 * need closing later.
 */
public final class ShutdownHook {

  private static final Logger logger = LoggerFactory.getLogger(ShutdownHook.class);
  private final List<AutoCloseable> autoCloseables;

  /** Creates a new shutdown hook register that hooks into the vm's shutdown. */
  public ShutdownHook() {
    this(Runtime.getRuntime()::addShutdownHook);
  }

  ShutdownHook(Consumer<Thread> shutdownHook) {
    autoCloseables = new ArrayList<>();
    shutdownHook.accept(new Thread(() -> stream().forEach(this::close)));
  }

  private void close(AutoCloseable autoCloseable) {
    logger.info("Running shutdown hook for {}", autoCloseable);
    ExceptionLogger.handle(logger::warn, autoCloseable::close, "Cannot close resource");
  }

  private Stream<AutoCloseable> stream() {
    return autoCloseables.stream();
  }

  /**
   * Gets the closeables connected to this shutdown hook.
   *
   * @return the list of closeables
   */
  public List<AutoCloseable> closeables() {
    return stream().toList();
  }

  /**
   * Register an executor service to be closed with this hook. This method is expected to be
   * deprecated in the future,
   *
   * @param service the service to close with this hook
   */
  public void register(ExecutorService service) {
    register(service::shutdownNow);
  }

  /**
   * Register an auto closeable to be closed with this hook.
   *
   * @param autoCloseable the closeable to close with this hook
   */
  public void register(AutoCloseable autoCloseable) {
    autoCloseables.add(autoCloseable);
  }
}
