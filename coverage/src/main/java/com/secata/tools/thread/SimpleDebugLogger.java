package com.secata.tools.thread;

import java.util.function.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class SimpleDebugLogger implements Consumer<Throwable> {

  private static final Logger logger = LoggerFactory.getLogger(SimpleDebugLogger.class);

  @Override
  public void accept(Throwable throwable) {
    logger.debug("Error", throwable);
  }
}
