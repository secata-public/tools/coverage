package com.secata.tools.immutable;

import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test of {@link FixedList}. */
public class FixedListTest {

  private static final FixedList<Integer> LIST_INTEGERS =
      FixedList.create(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));

  private static final FixedList<String> LIST_STRINGS =
      FixedList.create(List.of("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"));

  /** {@link FixedList} can be created from a normal list of elements. */
  @Test
  public void fromList() {
    FixedList<Integer> copyList = FixedList.create(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
    Assertions.assertThat(copyList).hasSize(10);
    Assertions.assertThat(copyList).containsExactly(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
  }

  /**
   * {@link FixedList} can be created from a {@link Collection} (including another {@link
   * FixedList}).
   */
  @Test
  public void fromCollection() {
    FixedList<Integer> copyList = FixedList.create(LIST_INTEGERS);
    Assertions.assertThat(copyList).hasSize(10);
    Assertions.assertThat(copyList).containsExactly(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
  }

  /** {@link FixedList} can be created from a finite stream of elements. */
  @Test
  public void fromStream() {
    FixedList<Integer> copyList = FixedList.create(LIST_INTEGERS.stream());
    Assertions.assertThat(copyList).hasSize(10);
    Assertions.assertThat(copyList).containsExactly(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
  }

  @Test
  public void rangeCheck() {
    Assertions.assertThatThrownBy(() -> LIST_INTEGERS.subList(-1, 5))
        .isInstanceOf(IndexOutOfBoundsException.class);
    Assertions.assertThatThrownBy(() -> LIST_INTEGERS.subList(5, 4))
        .isInstanceOf(IllegalArgumentException.class);
  }

  /** An empty {@link FixedList} can be extended with elements. */
  @Test
  public void createEmptyAndExtend() {
    FixedList<Integer> list = FixedList.create();
    Assertions.assertThat(list).hasSize(0);
    list = list.addElement(13);
    Assertions.assertThat(list).hasSize(1);
    Assertions.assertThat(list).contains(13);
  }

  @Test
  public void addElement() {
    FixedList<Integer> with42 = LIST_INTEGERS.addElement(42);
    Assertions.assertThat(with42).hasSize(11);
    Assertions.assertThat(LIST_INTEGERS).hasSize(10);
    Assertions.assertThat(with42).contains(42);
    Assertions.assertThat(with42.get(10)).isEqualTo(42);
  }

  @Test
  public void removeElement() {
    FixedList<Integer> without5 = LIST_INTEGERS.removeElement((Integer) 5);
    Assertions.assertThat(without5).hasSize(9);
    Assertions.assertThat(LIST_INTEGERS).hasSize(10);
    Assertions.assertThat(without5).containsExactly(1, 2, 3, 4, 6, 7, 8, 9, 10);
  }

  /** Removing an element not in the {@link FixedList} is an no-op. */
  @Test
  public void removeElementNonExistingInt() {
    Assertions.assertThat(LIST_INTEGERS.removeElement((Integer) 11))
        .hasSize(10)
        .isEqualTo(LIST_INTEGERS);
  }

  /** Removing an element not in the {@link FixedList} is an no-op. */
  @Test
  public void removeElementNonExistingStr() {
    Assertions.assertThat(LIST_STRINGS.removeElement("Test")).hasSize(12).isEqualTo(LIST_STRINGS);
  }

  /** Removing an element at an index, removes the element from the {@link FixedList}. */
  @Test
  public void removeElementAtIndex() {
    final FixedList<String> list = LIST_STRINGS.subList(0, 3);
    Assertions.assertThat(list.removeElementAtIndex(0)).containsExactly("B", "C");
    Assertions.assertThat(list.removeElementAtIndex(1)).containsExactly("A", "C");
    Assertions.assertThat(list.removeElementAtIndex(2)).containsExactly("A", "B");
  }

  /**
   * Removing an element at an index, removes the element from the {@link FixedList}, even when
   * using the deprecated variant.
   */
  @Test
  @SuppressWarnings({"deprecation", "InlineMeInliner"})
  public void removeElementAtIndexDeprecated() {
    final FixedList<String> list = LIST_STRINGS.subList(0, 3);
    Assertions.assertThat(list.removeElement(0)).containsExactly("B", "C");
    Assertions.assertThat(list.removeElement(1)).containsExactly("A", "C");
    Assertions.assertThat(list.removeElement(2)).containsExactly("A", "B");
  }

  /** Removing an element from a {@link FixedList#subList} works just like a normal removal. */
  @Test
  public void removeElementsFromSubList() {
    final FixedList<String> list = LIST_STRINGS.subList(4, 7);
    Assertions.assertThat(list.removeElementAtIndex(0)).containsExactly("F", "G");
    Assertions.assertThat(list.removeElementAtIndex(1)).containsExactly("E", "G");
    Assertions.assertThat(list.removeElementAtIndex(2)).containsExactly("E", "F");
  }

  @Test
  public void addElements() {
    FixedList<Integer> longer = LIST_INTEGERS.addElements(List.of(20, 21, 22, 25));
    Assertions.assertThat(longer).hasSize(14);
    Assertions.assertThat(LIST_INTEGERS).hasSize(10);
    Assertions.assertThat(longer).containsExactly(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 21, 22, 25);
  }

  @Test
  public void setElement() {
    FixedList<Integer> revised = LIST_INTEGERS.setElement(2, 50);
    Assertions.assertThat(revised).hasSize(10);
    Assertions.assertThat(LIST_INTEGERS).hasSize(10);
    Assertions.assertThat(revised).containsExactly(1, 2, 50, 4, 5, 6, 7, 8, 9, 10);
  }

  @Test
  public void subList() {
    Assertions.assertThat(LIST_INTEGERS.subList(1, 3)).hasSize(2);
    Assertions.assertThat(LIST_INTEGERS.subList(1, 3)).containsExactly(2, 3);
    Assertions.assertThat(LIST_INTEGERS.subList(1, 3).addElement(42)).containsExactly(2, 3, 42);
    Assertions.assertThat(LIST_INTEGERS.subList(1, 3).setElement(1, 42)).containsExactly(2, 42);

    Assertions.assertThat(LIST_INTEGERS.subList(0, 10)).hasSize(10);
    Assertions.assertThat(LIST_INTEGERS.subList(0, 10))
        .containsExactly(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

    Assertions.assertThat(LIST_INTEGERS.subList(2, 2)).hasSize(0);

    Assertions.assertThatThrownBy(() -> LIST_INTEGERS.subList(0, 11))
        .isInstanceOf(IndexOutOfBoundsException.class)
        .hasMessage("lastElementIndex = 11 data.length = 10");
  }

  @Test
  public void get() {
    testEntry(0, 1);
    testEntry(1, 2);
    testEntry(2, 3);
    testEntry(3, 4);
    testEntry(4, 5);
    testEntry(5, 6);
    testEntry(6, 7);
    testEntry(7, 8);
    testEntry(8, 9);
    testEntry(9, 10);
  }

  private void testEntry(int index, int expected) {
    Assertions.assertThat(LIST_INTEGERS.get(index)).isEqualTo(expected);
    Assertions.assertThat(LIST_INTEGERS.subList(index, index)).isEmpty();
    Assertions.assertThat(LIST_INTEGERS.subList(index, index + 1).get(0)).isEqualTo(expected);
    Assertions.assertThat(LIST_INTEGERS.subList(index, 10).get(0)).isEqualTo(expected);
    Assertions.assertThat(LIST_INTEGERS.subList(index, 10)).hasSize(10 - index);
    Assertions.assertThat(LIST_INTEGERS.setElement(index, 50).get(index)).isEqualTo(50);
    Assertions.assertThat(LIST_INTEGERS.subList(index, 10).setElement(0, 50).get(0)).isEqualTo(50);
  }

  @Test
  public void outOfBounds() {
    Assertions.assertThatThrownBy(() -> LIST_INTEGERS.get(-1))
        .isInstanceOf(IndexOutOfBoundsException.class);
    Assertions.assertThatThrownBy(() -> LIST_INTEGERS.get(10))
        .isInstanceOf(IndexOutOfBoundsException.class);
    Assertions.assertThatThrownBy(() -> LIST_INTEGERS.subList(5, 6).get(1))
        .isInstanceOf(IndexOutOfBoundsException.class);
    Assertions.assertThatThrownBy(() -> LIST_INTEGERS.subList(5, 6).setElement(1, 50))
        .isInstanceOf(IndexOutOfBoundsException.class);
    Assertions.assertThatThrownBy(() -> LIST_INTEGERS.subList(5, 6).setElement(-1, 50))
        .isInstanceOf(IndexOutOfBoundsException.class);
    Assertions.assertThatThrownBy(() -> LIST_INTEGERS.subList(4, 7).removeElementAtIndex(-1))
        .isInstanceOf(IndexOutOfBoundsException.class);
    Assertions.assertThatThrownBy(() -> LIST_INTEGERS.subList(4, 7).removeElementAtIndex(3))
        .isInstanceOf(IndexOutOfBoundsException.class);
    Assertions.assertThatThrownBy(() -> LIST_INTEGERS.removeElementAtIndex(11))
        .isInstanceOf(IndexOutOfBoundsException.class);
  }
}
