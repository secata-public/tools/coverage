package com.secata.tools.thread;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.concurrent.ExecutorService;
import org.junit.jupiter.api.Test;

/** Test. */
public class ExecutorFactoryTest {

  @Test
  public void nonNullValues() {
    List<ExecutorService> list =
        List.of(
            ExecutorFactory.newSingle("name"),
            ExecutorFactory.newCached("name"),
            ExecutorFactory.newScheduled("name", 2),
            ExecutorFactory.newScheduledSingleThread("name"));

    assertThat(list).doesNotContainNull();

    for (ExecutorService service : list) {
      service.shutdown();
    }
  }
}
