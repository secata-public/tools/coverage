package com.secata.tools.thread;

import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.ThrowingRunnable;
import java.time.Duration;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BooleanSupplier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

/** Test. */
public class ThreadedLoopTest {

  private static final String name = "ThreadedLoopTest";
  private ThreadedLoop thread;
  private ThreadedLoop threadedLoop;

  /** Stop any threads started. */
  @AfterEach
  public void tearDown() {
    if (this.thread != null) {
      this.thread.close();
    }
  }

  private ThreadedLoop startThread(ThrowingRunnable sleeping) {
    Assertions.assertThat(this.thread).isNull();

    ThreadedLoop start = ThreadedLoop.create(sleeping, name).start();
    this.thread = start;
    return start;
  }

  @Test
  public void runThreeTimes() throws InterruptedException {
    AtomicInteger count = new AtomicInteger(0);
    ThreadedLoop start = startThread(sleeping(count::incrementAndGet));
    Assertions.assertThat(start.isRunning()).isTrue();
    stopAbove(3, count, start).join(1000);
    start.join(30);
    Assertions.assertThat(start.isRunning()).isFalse();
    Assertions.assertThat(count.get()).isGreaterThanOrEqualTo(3);
  }

  private ThrowingRunnable sleeping(ThrowingRunnable inner) {
    return () -> {
      Thread.sleep(1);
      inner.run();
    };
  }

  private Thread stopAbove(int threshold, AtomicInteger count, ThreadedLoop start) {
    Thread watchDog =
        new Thread(
            () -> {
              int i = 0;
              while (count.get() < threshold && i++ < threshold) {
                ExceptionConverter.run(() -> Thread.sleep(50), "Error");
              }
              if (count.get() >= threshold) {
                start.close();
              }
            });
    watchDog.start();
    return watchDog;
  }

  @Test
  public void runLongWithErrors() throws InterruptedException {
    AtomicInteger exceptionCount = new AtomicInteger(0);
    AtomicInteger loggerCount = new AtomicInteger(0);
    AtomicInteger correctErrorTextCount = new AtomicInteger(0);
    AtomicInteger count = new AtomicInteger(0);
    ThrowingRunnable sleeping =
        sleeping(
            () -> {
              int currentCount = count.incrementAndGet();
              if (currentCount > 99) {
                thread.close();
              } else if (currentCount % 5 == 0) {
                throw new RuntimeException("Explicit test that each fifth iteration is broken");
              }
            });
    this.thread =
        ThreadedLoop.create(sleeping, name)
            .onException(exceptionCount::getAndIncrement)
            .logger(
                (s, e) -> {
                  loggerCount.incrementAndGet();
                  if (s.equals("Error") && e instanceof RuntimeException) {
                    correctErrorTextCount.incrementAndGet();
                  }
                },
                "Exception in ThreadedLoop")
            .start();
    thread.join(5000);
    Assertions.assertThat(count.get()).isEqualTo(100);
    Assertions.assertThat(exceptionCount.get()).isEqualTo(19);
    Assertions.assertThat(loggerCount.get()).isEqualTo(19);
  }

  @Test
  public void fastInitNoRun() throws InterruptedException {
    AtomicInteger count = new AtomicInteger(0);
    ThreadedLoop start =
        startThread(
            () -> {
              ExceptionConverter.run(() -> Thread.sleep(150), "Timeout error");
              count.incrementAndGet();
            });
    start.stop();
    start.join(10_000);
    Assertions.assertThat(count.get()).isEqualTo(0);
  }

  @Test
  public void interrupt() throws InterruptedException {
    AtomicBoolean started = new AtomicBoolean(false);
    AtomicBoolean interrupted = new AtomicBoolean(false);
    ThreadedLoop start =
        startThread(
            () -> {
              started.set(true);
              try {
                Thread.sleep(1500);
              } catch (InterruptedException e) {
                interrupted.set(true);
              }
            });
    waitForCondition(started::get);
    start.close();
    start.join(10_000);
    Assertions.assertThat(interrupted.get()).isEqualTo(true);
  }

  @Test
  public void runOnceFast() throws InterruptedException {
    AtomicBoolean started = new AtomicBoolean(false);
    ThreadedLoop start = startThread(() -> started.set(true));
    start.join(3);
    Assertions.assertThat(started.get()).isEqualTo(true);
  }

  private void waitForCondition(BooleanSupplier supplier) throws InterruptedException {
    long maxWaitTime = Duration.ofSeconds(60).toMillis();
    long timeout = System.currentTimeMillis() + maxWaitTime;
    while (System.currentTimeMillis() < timeout && !supplier.getAsBoolean()) {
      Thread.sleep(50);
    }
    Assertions.assertThat(supplier.getAsBoolean()).isTrue();
  }

  @Test
  public void testRun() {
    AtomicInteger runCount = new AtomicInteger(0);
    AtomicBoolean calledWhenStopped = new AtomicBoolean();
    threadedLoop =
        ThreadedLoop.create(
                () -> {
                  if (!threadedLoop.isRunning()) {
                    calledWhenStopped.set(true);
                  }
                  int count = runCount.incrementAndGet();
                  if (count > 4) {
                    threadedLoop.close();
                  }
                },
                name)
            .start();
    threadedLoop.run();
    Assertions.assertThat(calledWhenStopped.get()).isFalse();
    Assertions.assertThat(runCount.get()).isEqualTo(5);
    Assertions.assertThat(threadedLoop.isRunning()).isFalse();
  }

  @Test
  public void runCatchesThrowable() throws InterruptedException {
    AtomicBoolean onExceptionCalled = new AtomicBoolean(false);
    AtomicBoolean loggerCalled = new AtomicBoolean(false);
    OutOfMemoryError outOfMemoryError = new OutOfMemoryError("Throwable message");

    this.thread =
        ThreadedLoop.create(
                () -> {
                  throw outOfMemoryError;
                },
                name)
            .onException(() -> onExceptionCalled.set(true))
            .logger(
                (s, e) -> {
                  thread.close();
                  loggerCalled.set(true);
                  Assertions.assertThat(e).isSameAs(outOfMemoryError);
                },
                "")
            .start();
    thread.join(5000);

    Assertions.assertThat(onExceptionCalled).isTrue();
    Assertions.assertThat(loggerCalled).isTrue();
  }
}
