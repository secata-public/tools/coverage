package com.secata.tools.thread;

import java.lang.Thread.State;

final class ThreadTestHelper {

  static void waitForWaitingThenInterruptAndJoin(Thread thread) throws InterruptedException {
    for (int i = 0; i < 100; i++) {
      if (thread.getState().equals(State.WAITING)
          || thread.getState().equals(State.TIMED_WAITING)) {
        break;
      }
      Thread.sleep(300);
    }
    thread.interrupt();
    thread.join();
  }
}
