package com.secata.tools.thread;

import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.thread.SharedPoolSingleThreadedExecution.SingleThreadExecutor;
import com.secata.tools.thread.SharedPoolSingleThreadedExecution.Work;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class SharedPoolSingleThreadedExecutionTest {

  private static final String name = "SharedPoolSingleThreadedExecutionTest";

  @Test
  public void putInDead() {
    SharedPoolSingleThreadedExecution execution = SharedPoolSingleThreadedExecution.named(name);
    execution.close();
    Callable<Object> runnable = () -> null;
    SingleThreadExecutor singleThreadedExecutor = execution.createSingleThreadedExecutor();
    Assertions.assertThat(singleThreadedExecutor.isAlive()).isFalse();
    Future<Object> execute = singleThreadedExecutor.execute(runnable);
    Assertions.assertThat(execute).isNotNull();
    Assertions.assertThatThrownBy(execute::get)
        .isInstanceOf(ExecutionException.class)
        .hasMessage("java.lang.RuntimeException: Executor is closed");
  }

  @Test
  public void checkRunnable() throws InterruptedException {
    SharedPoolSingleThreadedExecution execution = SharedPoolSingleThreadedExecution.named(name);

    AtomicBoolean atomicBoolean = new AtomicBoolean();
    Runnable runnable = () -> ExceptionConverter.run(() -> atomicBoolean.set(true), "Failed");
    execution.createSingleThreadedExecutor().execute(runnable);
    for (int i = 0; !atomicBoolean.get() && i < 10; i++) {
      Thread.sleep(100);
    }
    Assertions.assertThat(atomicBoolean).isTrue();
  }

  @Test
  public void getResultFromDead() {
    SharedPoolSingleThreadedExecution execution = SharedPoolSingleThreadedExecution.named(name);

    Callable<Void> runnable =
        () -> {
          Thread.sleep(10000);
          return null;
        };
    Future<Void> work = execution.createSingleThreadedExecutor().execute(runnable);
    execution.close();
    Assertions.assertThatThrownBy(work::get)
        .isInstanceOf(ExecutionException.class)
        .cause()
        .isInstanceOf(InterruptedException.class);
  }

  @Test
  public void getResultInterrupted() {
    SharedPoolSingleThreadedExecution execution = SharedPoolSingleThreadedExecution.named(name);

    Callable<Object> runnable =
        () -> {
          Thread.sleep(10000);
          return null;
        };
    Future<Object> work = execution.createSingleThreadedExecutor().execute(runnable);
    Thread testThread = Thread.currentThread();
    Thread thread =
        new Thread(
            () -> {
              try {
                ThreadTestHelper.waitForWaitingThenInterruptAndJoin(testThread);
              } catch (InterruptedException e) {
                throw new RuntimeException(e);
              }
            });
    thread.start();

    Assertions.assertThatThrownBy(work::get).isInstanceOf(InterruptedException.class);
  }

  @Test
  public void getResultThrowing() {
    SharedPoolSingleThreadedExecution execution = SharedPoolSingleThreadedExecution.named(name);

    final NullPointerException expected = new NullPointerException("expected");
    Callable<Object> runnable =
        () -> {
          throw expected;
        };
    SingleThreadExecutor singleThreadedExecutor = execution.createSingleThreadedExecutor();
    Future<Object> work = singleThreadedExecutor.execute(runnable);
    try {
      work.get();
      Assertions.fail("Exception expected");
    } catch (Exception e) {
      Assertions.assertThat(e.getCause()).isSameAs(expected);
    }
  }

  @Test
  public void getResult() throws Exception {
    SharedPoolSingleThreadedExecution execution = SharedPoolSingleThreadedExecution.named(name);

    String expected = "My expected string";
    Callable<String> runnable = () -> expected;
    SingleThreadExecutor singleThreadedExecutor = execution.createSingleThreadedExecutor();
    Future<String> work = singleThreadedExecutor.execute(runnable);

    Assertions.assertThat(work.get()).isEqualTo(expected);
  }

  @Test
  public void checkMoreWork() throws Exception {
    SharedPoolSingleThreadedExecution execution = SharedPoolSingleThreadedExecution.named(name);

    String expected = "My expected string";
    Callable<String> runnable = () -> expected;
    SingleThreadExecutor singleThreadedExecutor = execution.createSingleThreadedExecutor();

    Future<String> work1 = singleThreadedExecutor.execute(runnable);
    Future<String> work2 = singleThreadedExecutor.execute(runnable);

    Assertions.assertThat(work1.get()).isEqualTo(expected);
    Assertions.assertThat(work2.get()).isEqualTo(expected);
  }

  @Test
  public void start() {
    AtomicBoolean started = new AtomicBoolean(false);
    AtomicBoolean checkForMore = new AtomicBoolean(false);

    Work<Void> work =
        new Work<>(
            () -> {
              started.set(true);
              return null;
            },
            () -> checkForMore.set(true));

    work.start(Runnable::run);
    Assertions.assertThat(started.get()).isTrue();
    Assertions.assertThat(checkForMore.get()).isTrue();
  }
}
