package com.secata.tools.thread;

import static org.assertj.core.api.Assertions.assertThat;

import com.secata.tools.coverage.ThrowingRunnable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public class ExecutorFactoryThreadedTest {

  private static final String THREAD_NAME = "name";
  private ExecutionFactoryThreaded executorFactory;

  @BeforeEach
  void setUp() {
    executorFactory = ExecutionFactoryThreaded.create();
  }

  @Test
  void defaultCreate() {
    assertThat(ExecutionFactoryThreaded.create()).isNotNull();
  }

  @Test
  public void newSingleThreadExecutor() throws Exception {
    testExecutorService(executorFactory.newSingleThreadExecutor(THREAD_NAME));
  }

  @Test
  public void newCachedThreadPool() throws Exception {
    testExecutorService(executorFactory.newCachedThreadPool(THREAD_NAME));
  }

  @Test
  public void newScheduledThreadPool() throws Exception {
    ScheduledExecutorService service = executorFactory.newScheduledThreadPool(THREAD_NAME, 1);
    testExecutorService(service);
  }

  @Test
  public void newSingleThreadScheduledExecutor() throws Exception {
    ScheduledExecutorService service =
        executorFactory.newSingleThreadScheduledExecutor(THREAD_NAME);
    testExecutorService(service);
  }

  private void testExecutorService(ExecutorService service) throws Exception {
    assertThat(service).isNotNull();
    assertThat(service.isShutdown()).isFalse();
    CountDownLatch count = new CountDownLatch(1);
    service.execute(new CountIfCorrectName(count));
    assertThat(count.await(1, TimeUnit.SECONDS)).isTrue();
    service.shutdown();
    assertThat(service.isShutdown()).isTrue();
  }

  @Test
  void threadedLoopRuns() throws InterruptedException {
    testThreadLoopRun(1);
    testThreadLoopRun(15);
  }

  private void testThreadLoopRun(int count) throws InterruptedException {
    CountDownLatch countDownLatch = new CountDownLatch(count);
    ContinuousExecution threadedLoop =
        executorFactory
            .newContinuousExecution(THREAD_NAME, new CountIfCorrectName(countDownLatch))
            .start();
    assertThat(countDownLatch.await(1, TimeUnit.SECONDS)).isTrue();
    assertThat(threadedLoop.isRunning()).isTrue();
    threadedLoop.close();
    assertThat(threadedLoop.isRunning()).isFalse();
  }

  @Test
  void threadedLoopExceptionSimpleLogger() throws InterruptedException {
    CountDownLatch exceptionCheck = new CountDownLatch(1);
    CountDownLatch loggerCheck = new CountDownLatch(1);
    ContinuousExecution threadedLoop =
        executorFactory
            .newContinuousExecution(
                "test-thread",
                () -> {
                  throw new RuntimeException();
                })
            .onException(exceptionCheck::countDown)
            .logger(e -> loggerCheck.countDown())
            .start();
    assertThat(exceptionCheck.await(1, TimeUnit.SECONDS)).isTrue();
    assertThat(loggerCheck.await(1, TimeUnit.SECONDS)).isTrue();
    assertThat(threadedLoop.isRunning()).isTrue();
    threadedLoop.close();
  }

  @Test
  public void threadedLoopCatchesThrowableComplexLogger() throws InterruptedException {
    CountDownLatch exceptionCheck = new CountDownLatch(1);
    CountDownLatch loggerCheck = new CountDownLatch(1);
    OutOfMemoryError outOfMemoryError = new OutOfMemoryError("Throwable message");

    ContinuousExecution.Builder builder =
        executorFactory
            .newContinuousExecution(
                "Thread Name",
                () -> {
                  throw outOfMemoryError;
                })
            .onException(exceptionCheck::countDown)
            .logger(
                (s, e) -> {
                  loggerCheck.countDown();
                  assertThat(e).isSameAs(outOfMemoryError);
                },
                "Empty");
    try (ContinuousExecution thread = builder.start()) {
      assertThat(thread.isRunning()).isTrue();
      Thread.sleep(50);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    assertThat(exceptionCheck.await(1, TimeUnit.SECONDS)).isTrue();
    assertThat(loggerCheck.await(1, TimeUnit.SECONDS)).isTrue();
  }

  @Test
  void newSharedPoolSingleThreaded() throws Exception {
    SharedPoolSingleThreadedExecution execution =
        executorFactory.newSharedPoolSingleThreaded(THREAD_NAME);

    SharedPoolSingleThreadedExecution.SingleThreadExecutor threadedExecutor =
        execution.createSingleThreadedExecutor();
    assertThat(threadedExecutor).isNotNull();
    assertThat(threadedExecutor.isAlive()).isTrue();
    CountDownLatch countDownLatch = new CountDownLatch(1);
    threadedExecutor.execute(new CountIfCorrectName(countDownLatch, "SPST-" + THREAD_NAME));
    assertThat(countDownLatch.await(1, TimeUnit.SECONDS)).isTrue();
    execution.close();
    assertThat(threadedExecutor.isAlive()).isFalse();
  }

  private static class CountIfCorrectName implements Runnable, ThrowingRunnable {

    private final CountDownLatch countDownLatch;
    private final String correctPrefix;

    public CountIfCorrectName(CountDownLatch countDownLatch) {
      this(countDownLatch, THREAD_NAME + "-");
    }

    public CountIfCorrectName(CountDownLatch countDownLatch, String correctPrefix) {
      this.countDownLatch = countDownLatch;
      this.correctPrefix = correctPrefix;
    }

    @Override
    public void run() {
      if (Thread.currentThread().getName().startsWith(correctPrefix)) {
        countDownLatch.countDown();
      }
    }
  }
}
