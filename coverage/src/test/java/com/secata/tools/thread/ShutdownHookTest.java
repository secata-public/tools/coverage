package com.secata.tools.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public class ShutdownHookTest {

  @Test
  public void defaultConstructor() {
    ShutdownHook shutdownHook = new ShutdownHook();
    Assertions.assertThat(shutdownHook).isNotNull().isInstanceOf(ShutdownHook.class);
  }

  @Test
  public void shutdownHook() throws InterruptedException {
    List<Thread> shutdownHooks = new ArrayList<>();
    ShutdownHook shutdownHook = new ShutdownHook(shutdownHooks::add);
    AtomicBoolean closed = new AtomicBoolean();
    Assertions.assertThat(shutdownHook.closeables()).hasSize(0);
    AutoCloseable autoCloseable = () -> closed.set(true);
    shutdownHook.register(autoCloseable);
    Assertions.assertThat(shutdownHook.closeables()).hasSize(1);
    Assertions.assertThat(shutdownHooks).hasSize(1);
    Thread shutdownHookThread = shutdownHooks.get(0);
    shutdownHookThread.start();
    shutdownHookThread.join();

    Assertions.assertThat(closed).isTrue();
  }

  @Test
  public void shutdownService() throws InterruptedException {
    List<Thread> shutdownHooks = new ArrayList<>();
    ShutdownHook shutdownHook = new ShutdownHook(shutdownHooks::add);
    ExecutorService test = ExecutorFactory.newSingle("Test");
    Assertions.assertThat(shutdownHook.closeables()).hasSize(0);
    shutdownHook.register(test);
    Assertions.assertThat(shutdownHook.closeables()).hasSize(1);
    Assertions.assertThat(shutdownHooks).hasSize(1);
    Thread shutdownHookThread = shutdownHooks.get(0);
    shutdownHookThread.start();
    shutdownHookThread.join();

    Assertions.assertThat(test.isShutdown()).isTrue();
  }
}
