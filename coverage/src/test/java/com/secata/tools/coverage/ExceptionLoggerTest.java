package com.secata.tools.coverage;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.atomic.AtomicBoolean;
import org.junit.jupiter.api.Test;

/** Test. */
public class ExceptionLoggerTest {

  @Test
  public void shouldNotCallLoggerWhenNoError() {
    AtomicBoolean loggerCalled = new AtomicBoolean(false);
    AtomicBoolean methodCalled = new AtomicBoolean(false);
    ExceptionLogger.handle(
        (s, e) -> loggerCalled.set(true), () -> methodCalled.set(true), "Should not be seen");
    assertThat(loggerCalled).isFalse();
    assertThat(methodCalled).isTrue();
  }

  @Test
  public void shouldCallLoggerOnError() {
    AtomicBoolean loggerCalled = new AtomicBoolean(false);
    AtomicBoolean methodCalled = new AtomicBoolean(false);
    RuntimeException runtimeException = new RuntimeException();
    String message = "I am message";
    ExceptionLogger.handle(
        (s, e) -> {
          loggerCalled.set(true);
          assertThat(e).isSameAs(runtimeException);
          assertThat(s).isEqualTo(message);
        },
        () -> {
          methodCalled.set(true);
          throw runtimeException;
        },
        message);
    assertThat(loggerCalled).isTrue();
    assertThat(methodCalled).isTrue();
  }

  @Test
  public void shouldCallLoggerOnThrowable() {
    AtomicBoolean loggerCalled = new AtomicBoolean(false);
    AtomicBoolean methodCalled = new AtomicBoolean(false);
    String message = "Throwable message";
    OutOfMemoryError outOfMemoryError = new OutOfMemoryError(message);
    ExceptionLogger.handle(
        (s, e) -> {
          loggerCalled.set(true);
          assertThat(e).isSameAs(outOfMemoryError);
          assertThat(s).isEqualTo(message);
        },
        () -> {
          methodCalled.set(true);
          throw outOfMemoryError;
        },
        message);
    assertThat(loggerCalled).isTrue();
    assertThat(methodCalled).isTrue();
  }
}
