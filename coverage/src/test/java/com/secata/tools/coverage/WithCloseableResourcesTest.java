package com.secata.tools.coverage;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public class WithCloseableResourcesTest {

  @Test
  public void close() {
    AtomicBoolean closedCalled = new AtomicBoolean();
    WithCloseableResources closeableResources =
        () -> Stream.of((AutoCloseable) () -> closedCalled.set(true));
    Assertions.assertThat(closedCalled.get()).isFalse();
    closeableResources.close();
    Assertions.assertThat(closedCalled.get()).isTrue();
  }

  @Test
  public void resources() {
    List<AutoCloseable> closeables = List.of();
    WithCloseableResources closeableResources = closeables::stream;
    Assertions.assertThat(closeableResources.resources()).hasSize(0);
  }
}
