package com.secata.tools.coverage;

import java.util.concurrent.Callable;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public class ExceptionConverterTest {

  @Test
  public void safeCall() {
    Integer result = ExceptionConverter.call(() -> 42);
    Assertions.assertThat(result).isEqualTo(42);
  }

  @Test
  public void safeCallMessage() {
    Integer result = ExceptionConverter.call(() -> 42, "Ignored");
    Assertions.assertThat(result).isEqualTo(42);
  }

  @Test
  public void safeCallFailing() {
    Assertions.assertThatThrownBy(
            () ->
                ExceptionConverter.call(
                    (Callable<?>)
                        () -> {
                          throw new Exception();
                        }))
        .isInstanceOf(RuntimeException.class);
  }

  @Test
  public void safeCallFailingMessage() {
    Assertions.assertThatThrownBy(
            () ->
                ExceptionConverter.call(
                    (Callable<?>)
                        () -> {
                          throw new Exception();
                        },
                    "Ignored"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Ignored");
  }

  @Test
  public void safeRun() {
    boolean[] run = new boolean[1];
    ExceptionConverter.run(() -> run[0] = true);
    Assertions.assertThat(run[0]).isTrue();
  }

  @Test
  public void safeRunMessage() {
    boolean[] run = new boolean[1];
    ExceptionConverter.run(() -> run[0] = true, "Ignored");
    Assertions.assertThat(run[0]).isTrue();
  }

  @Test
  public void safeRunFailing() {
    Assertions.assertThatThrownBy(
            () ->
                ExceptionConverter.run(
                    () -> {
                      throw new Exception();
                    }))
        .isInstanceOf(RuntimeException.class);
  }

  @Test
  public void safeRunFailingMessage() {
    Assertions.assertThatThrownBy(
            () ->
                ExceptionConverter.run(
                    () -> {
                      throw new Exception();
                    },
                    "Ignored"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Ignored");
  }
}
