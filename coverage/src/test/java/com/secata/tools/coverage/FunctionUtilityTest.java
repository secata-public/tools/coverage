package com.secata.tools.coverage;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public class FunctionUtilityTest {

  @Test
  public void nullFunction() {
    Function<String, Long> function = FunctionUtility.nullFunction();
    Assertions.assertThat(function).isNotNull();
    Assertions.assertThat(function.apply("input")).isNull();
  }

  @Test
  public void nullBiFunction() {
    BiFunction<String, Integer, Long> function = FunctionUtility.nullBiFunction();
    Assertions.assertThat(function).isNotNull();
    Assertions.assertThat(function.apply("input", 77)).isNull();
  }

  @Test
  public void nullSupplier() {
    Supplier<String> function = FunctionUtility.nullSupplier();
    Assertions.assertThat(function).isNotNull();
    Assertions.assertThat(function.get()).isNull();
  }

  @Test
  public void noOpBiConsumer() {
    BiConsumer<String, Long> consumer = FunctionUtility.noOpBiConsumer();
    Assertions.assertThat(consumer).isNotNull();
    consumer.accept("input", 73L);
  }

  @Test
  public void noOpConsumer() {
    Consumer<String> consumer = FunctionUtility.noOpConsumer();
    Assertions.assertThat(consumer).isNotNull();
    consumer.accept("input");
  }

  @Test
  public void noOpRunnable() {
    Runnable runnable = FunctionUtility.noOpRunnable();
    Assertions.assertThat(runnable).isNotNull();
    runnable.run();
  }
}
