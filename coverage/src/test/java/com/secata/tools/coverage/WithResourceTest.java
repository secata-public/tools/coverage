package com.secata.tools.coverage;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
@SuppressWarnings("resource")
public class WithResourceTest {

  @Test
  public void accept() {
    MyCloseable closeable = new MyCloseable();

    WithResource.accept(() -> closeable, x -> x.value = 1);

    Assertions.assertThat(closeable.closed).isTrue();
    Assertions.assertThat(closeable.value).isEqualTo(1);
  }

  @Test
  public void acceptMessage() {
    MyCloseable closeable = new MyCloseable();

    WithResource.accept(() -> closeable, x -> x.value = 1, "Not triggered");

    Assertions.assertThat(closeable.closed).isTrue();
    Assertions.assertThat(closeable.value).isEqualTo(1);
  }

  @Test
  public void apply() {
    MyCloseable closeable = new MyCloseable();

    int value = WithResource.apply(() -> closeable, x -> 42);

    Assertions.assertThat(closeable.closed).isTrue();
    Assertions.assertThat(closeable.value).isEqualTo(0);
    Assertions.assertThat(value).isEqualTo(42);
  }

  @Test
  public void applyMessage() {
    MyCloseable closeable = new MyCloseable();

    int value = WithResource.apply(() -> closeable, x -> 42, "Not triggered");

    Assertions.assertThat(closeable.closed).isTrue();
    Assertions.assertThat(closeable.value).isEqualTo(0);
    Assertions.assertThat(value).isEqualTo(42);
  }

  @Test
  public void acceptWithError() {
    MyCloseable closeable = new MyCloseable();

    Assertions.assertThatThrownBy(
            () ->
                WithResource.accept(
                    () -> closeable,
                    x -> {
                      throw new IllegalStateException("Test of error");
                    }))
        .hasMessage("java.lang.IllegalStateException: Test of error")
        .isInstanceOf(RuntimeException.class);

    Assertions.assertThat(closeable.closed).isTrue();
    Assertions.assertThat(closeable.value).isEqualTo(0);
  }

  @Test
  public void acceptMessageWithError() {
    MyCloseable closeable = new MyCloseable();

    IllegalStateException cause = new IllegalStateException("Test of error");
    Assertions.assertThatThrownBy(
            () ->
                WithResource.accept(
                    () -> closeable,
                    x -> {
                      throw cause;
                    },
                    "Extra Message"))
        .hasMessage("Extra Message")
        .isInstanceOf(RuntimeException.class)
        .hasCause(cause);

    Assertions.assertThat(closeable.closed).isTrue();
    Assertions.assertThat(closeable.value).isEqualTo(0);
  }

  @Test
  public void acceptWithErrorInClose() {
    RuntimeException supressed = new RuntimeException("Intended exception");
    AutoCloseable closeable =
        () -> {
          throw supressed;
        };

    IllegalStateException cause = new IllegalStateException("Hest");
    Assertions.assertThatThrownBy(
            () ->
                WithResource.accept(
                    () -> closeable,
                    x -> {
                      throw cause;
                    },
                    "Extra Message"))
        .hasMessage("Extra Message")
        .isInstanceOf(RuntimeException.class)
        .hasCause(cause);

    Assertions.assertThat(cause).hasSuppressedException(supressed);
  }

  @Test
  public void applyWithError() {
    MyCloseable closeable = new MyCloseable();
    IllegalStateException exception = new IllegalStateException("Test of error");
    Assertions.assertThatThrownBy(
            () ->
                WithResource.apply(
                    () -> closeable,
                    x -> {
                      throw exception;
                    },
                    "Extra Message"))
        .hasMessage("Extra Message")
        .isInstanceOf(RuntimeException.class)
        .hasCause(exception);

    Assertions.assertThat(closeable.closed).isTrue();
    Assertions.assertThat(closeable.value).isEqualTo(0);
  }

  @Test
  public void applyMessageWithErrorInClose() {
    RuntimeException supressed = new RuntimeException("Intended exception");
    AutoCloseable closeable =
        () -> {
          throw supressed;
        };

    IllegalStateException cause = new IllegalStateException("Test of error");
    Assertions.assertThatThrownBy(
            () ->
                WithResource.apply(
                    () -> closeable,
                    x -> {
                      throw cause;
                    },
                    "Extra Message"))
        .hasMessage("Extra Message")
        .isInstanceOf(RuntimeException.class)
        .hasCause(cause);

    Assertions.assertThat(cause).hasSuppressedException(supressed);
  }

  @Test
  public void applyWithErrorInClose() {
    RuntimeException supressed = new RuntimeException("Intended exception");
    AutoCloseable closeable =
        () -> {
          throw supressed;
        };

    IllegalStateException cause = new IllegalStateException("Test of error");
    Assertions.assertThatThrownBy(
            () ->
                WithResource.apply(
                    () -> closeable,
                    x -> {
                      throw cause;
                    }))
        .hasMessage("java.lang.IllegalStateException: Test of error")
        .isInstanceOf(RuntimeException.class)
        .hasCause(cause);

    Assertions.assertThat(cause).hasSuppressedException(supressed);
  }

  private static final class MyCloseable implements AutoCloseable {

    private boolean closed;
    private int value;

    @Override
    public void close() {
      closed = true;
    }
  }
}
